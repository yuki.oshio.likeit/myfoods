package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserDeleteServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//前の画面のデータid及びloginIdを取得
		String id = request.getParameter("id");
		UserDao dao = new UserDao();
		String loginId = dao.searchlogId(id);

		//sessionスコープにidをセット
		request.setAttribute("id", id);

		//確認用のlogin_id(削除される予定の方の)をセット
		request.setAttribute("loginId", loginId);
		//userdelete.jsp画面表示
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdelete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		String loginId = loginUser.getLoginId();

		//管理者の場合、userListへ
		if (loginId.equals("admin")) {
			//前の画面のデータ取得
			String id = request.getParameter("id");

			//sql処理(ここでは削除処理)
			UserDao dao = new UserDao();
			dao.userdelete(id);

			session.setAttribute("Msg", "削除を完了しました。");
			//リダイレクト
			response.sendRedirect("UserListServlet");
		} else {
			//前の画面のデータ取得
			String id = request.getParameter("id");
			//sql処理(ここでは削除処理)
			UserDao dao = new UserDao();
			dao.userdelete(id);

			session.setAttribute("Msg", "削除を完了しました。");
			//リダイレクト
			response.sendRedirect("UserDeleteResultServlet");
		}

	}

}
