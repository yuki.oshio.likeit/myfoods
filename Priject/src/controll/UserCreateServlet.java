package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserCreateServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// ユーザ一覧情報を取得
		HttpSession session = request.getSession();
		// リクエストスコープにユーザ一覧情報をセット
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/register.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		//文字コード設定
		request.setCharacterEncoding("UTF-8");

		//getParameter()で入力項目取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("nicname");
		String password = request.getParameter("password");
		String passwordkakunin = request.getParameter("passwordkakunin");
		String mail = request.getParameter("mail");

		//同じログインidないか探す
		UserDao userDao = new UserDao();
		String e = userDao.searchId(loginId);
		//passwordとpasswordkakuninが一致しなかった場合または↑で取得されるべき項目が1つでも空だったら　Stringがからの場合nullではなく「""」が入る（空文字）のでequals("")としている
		if (!(password.equals(passwordkakunin)) || e != null || loginId.equals("") || name.equals("")
				|| password.equals("") || passwordkakunin.equals("") || mail.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力されていない項目があるか、既に使われているログインIDか、入力されたパスワードが正しくありません。");
			//register.jspにフォワードして戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/register.jsp");
			dispatcher.forward(request, response);
			return;

		} else {
			//取得した入力項目を入れた引数を使い、DAOに入れる
			UserDao dao = new UserDao();
			dao.useradd(loginId, name, mail, password);
			User loginUser = dao.loginsessions(loginId, password);
			session.setAttribute("loginUser", loginUser);
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserCreateResultServlet");
		}
	}

}
