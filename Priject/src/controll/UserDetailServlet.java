package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserDetailServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//セッションスコープからuserid,loginidの入ったインスタンスを取得
		User loginUser = (User) session.getAttribute("loginUser");
		//findall()で検索し入れ込んであるデータをこのサーブレットのセッションにセット
		UserDao userDao = new UserDao();

		User user = userDao.findthree(loginUser.getLoginId());

		// リクエストスコープにユーザ一覧情報をセットここでまとまったインスタンスuserをセット
		request.setAttribute("user", user);

		//userdetailを表示させる
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
		dispatcher.forward(request, response);

		session.removeAttribute("errMsg");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.sendRedirect("UserUpdateServlet");
	}

}
