package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

@WebServlet("/UserCreateResultServlet")
public class UserCreateResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserCreateResultServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		//sessionのloginUserインスタンス取得
		User loginUser = (User) session.getAttribute("loginUser");

		//findall()で検索し入れ込んであるデータをこのサーブレットのセッションにセット
		UserDao userDao = new UserDao();
		User user = userDao.findrow(loginUser.getLoginId());

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		//usercreateresult.jsp画面表示
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usercreateresult.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usercreateresult.jsp");
		dispatcher.forward(request, response);

	}

}
