package controll;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.Material;
import beans.Processset;
import beans.Recipe;
import beans.Tag;
import dao.MaterialDao;
import dao.ProcessDao;
import dao.RecipeinsertDao;
import dao.RecipetagDao;
import dao.UserDao;

@WebServlet("/OneFoodUpdateServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-0CS8D75.000.001\\Documents\\myfoods\\Priject\\WebContent\\img", maxFileSize = 5242880)

public class OneFoodUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public OneFoodUpdateServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
		//ユーザーid取得

		//レシピid取得
		String preid = request.getParameter("id");
		int recipeid=Integer.parseInt(preid);

		//このサーブレットのdopostメソッドにidを渡して使えるようにするために、JSPに書いたhiddenタグに用意した変数にデータをセットする
		request.setAttribute("id", recipeid);

		//idを基に検索、料理の詳細を表示
		//使うdaoインスタンス作
		RecipeinsertDao recipe=new RecipeinsertDao();
		Recipe recipeList= recipe.outputrecipeinsert(recipeid);

		UserDao user=new UserDao();
		String nicname=user.searchnicnamebyuserid(recipeList.getUsersid());
		//recipeidを元に調理手順情報を取得
		ProcessDao process=new ProcessDao();
		List<Processset>processList=new ArrayList<Processset>();
		processList=process.setProcessListByRecipeId(recipeid);
		int processnumber=0;


		//recipeidを元にタグの名前を取得
		RecipetagDao tag=new RecipetagDao();
		List<Tag>tagList=new ArrayList<Tag>();
		tagList=tag.setTagListByRecipeId(recipeid);
		//recipeidを元に材料情報を取得
		MaterialDao material=new MaterialDao();
		List<Material>materialList=new ArrayList<Material>();
		materialList=material.setMaterialListByRecipeId(recipeid);

		//各種↑で情報を入れ込んだ対応するbeansのインスタンスをJSPにセット。
		request.setAttribute("processList", processList);	//processbeansインスタンス
		request.setAttribute("usernicname",nicname );	//usernicnameが入っている
		request.setAttribute("recipedetail",recipeList );	//recipeinsertbeansインスタンス
		request.setAttribute("materialList",materialList );	//materialbeansインスタンス
		request.setAttribute("tagList",tagList );	//tagbeansインスタンス

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/onefoodupdate.jsp");
		dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		//ここからinsertの模倣

		HttpSession session = request.getSession();
		try {
			//2.ユーザー情報をgetParameter()で入力項目取得 ここの変数名はloginsessions()メソッドの引数と同じにする。入力された文字列を用意した変数に入れ込み、
			//レシピid取得
			String preid = request.getParameter("id");
			int recipeid=Integer.parseInt(preid);

			//1.料理投稿画面入力情報をgetParameter()で取得
			//画像を取得するためのメソッド専用のPart型の変数を作り、nameをget専用型で取得。

			//String foodimgpass=request.getParameter("imgpass");
			String foodname = request.getParameter("foodname"); //料理名
			String time = request.getParameter("time"); //所要時間
			String cost = request.getParameter("cost"); //費用
			String cal = request.getParameter("cal"); //カロリー
			String comment = request.getParameter("comment"); //コメント
			String servings = request.getParameter("servings"); //人前

			//タグの取得
			//タグはJSPのnameではname1~12で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。12個までのタグ数制限設定なので配列は12で設定。
			//もし入力欄に1つ飛ばされて入力された場合の処理をif文で分けて、引っかかったらcontinueで飛ばしてforの処理を引っかかったところだけ飛ばして処理する。
			String[] tagList = new String[12];
			for (int i = 0; i < tagList.length; i++) {
				tagList[i] = request.getParameter("tag" + (i + 1));
			}


			//材料名の取得
			//タグはJSPのnameではmaterialname1~12で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。12個までのタグ数制限設定なので配列は12で設定。
			String[] materialList = new String[12];
			for (int i = 0; i < materialList.length; i++) {
				materialList[i] = request.getParameter("materialname" + (i + 1));
			}



			//分量の取得
			//分量はJSPのnameではquantity1~12で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。12個までのタグ数制限設定なので配列は12で設定。
			String[] quantityList = new String[12];
			for (int i = 0; i < quantityList.length; i++) {
				quantityList[i] = request.getParameter("quantity" + (i + 1));
			}


			//調理手順の取得。
			//調理手順はJSPのnameではname1~7で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。7個までの設定なので配列は7で設定。
			String[] processList = new String[7];
			for (int i = 0; i < processList.length; i++) {
				processList[i] = request.getParameter("process" + (i + 1));
			}



			//2.上の１．で取得し、入れ込んだ変数を対応するbeansのインスタンスに入れ込む。
			//そのためのインスタンスを用意。Recipeinsertテーブルに入れるのでそのテーブル名と同名のbeans名で作ったbeans型のインスタンスを用意する。
			Recipe toukou = new Recipe();

			//part.write(fileName);//画像を書き込む為のpart宣言
			//toukou.setFoodimgpass(fileName);//Recipeインスタンスtoukouにファイル名をセット
			Part part = request.getPart("newimgpass"); //料理画像 インポートの際にsunの付くものはインポートしない。
			if (part.getSize() > 0) {
				//↓新しく画像ファイル(画像情報)をフォルダに入れる必要があるためwriteメソッドで書き込んでいる。
				part.write(this.getFileName(part));
				//取得した画像情報(ファイル名)をstrin型のfileNameとして扱えるように
				String fileName = this.getFileName(part);
				toukou.setFoodimgpass(fileName);
			} else {
				Part part2 = request.getPart("imgpass");
				//取得した画像情報をstrin型のfileNameとして扱えるように
				String fileName=this.getFileName(part2);
				toukou.setFoodimgpass(fileName);
			}

			toukou.setId(recipeid);
			toukou.setFoodname(foodname);
			toukou.setTime(time);
			toukou.setCost(cost);
			toukou.setCal(cal);
			toukou.setComments(comment);
			toukou.setServings(servings);

			//入力された主なresipe内容を登録
			RecipeinsertDao.recipeinsertupdate(toukou);

			//tag繰り返し更新
			RecipetagDao.recipetagdeleteup(recipeid);
			for (int i = 0; i < tagList.length; i++) {
				//空文字を入れたくないのでcontinueで弾く
				if(!tagList[i].equals("")&&tagList[i]!=null){
					Tag recipe = new Tag();
					recipe.setRecipeid(recipeid);
					recipe.setTagname(tagList[i]);
					RecipetagDao.taginsert(recipe);
				}else {
					continue;
				}
			}

			//材料と分量の更新
			MaterialDao.materialdeleteup(recipeid);
			for (int i = 0; i < materialList.length; i++) {
				Material recipe = new Material();
				//空文字を入れたくないのでcontinueで弾く
				if(!materialList[i].equals("")&&materialList[i]!=null&&!quantityList[i].equals("")&&quantityList[i]!=null){
					recipe.setRecipeid(recipeid);
					recipe.setMaterial(materialList[i]);
					recipe.setQuantity(quantityList[i]);
					MaterialDao.materialinsert(recipe);
				}else {
					continue;
				}

			}


			//参考画像の更新
			ProcessDao.processdeleteup(recipeid);
			Part[] processimgpart = new Part[7];
			for (int i = 0; i < processList.length; i++) {
				//空文字を入れたくないのでcontinueで弾く
				if(!processList[i].equals("")&&processList[i]==null){
					continue;
				}
				Processset recipe = new Processset();
				recipe.setRecipeid(recipeid);
				processimgpart[i] = request.getPart("processimgpass" + (i + 1));
				if (processimgpart[i].getSize() > 0) {
					//↓新しく画像ファイルをフォルダに入れる
					processimgpart[i].write(this.getFileName(processimgpart[i]));
					recipe.setProcessimgpass(this.getFileName(processimgpart[i]));

				} else {

					recipe.setProcessimgpass(request.getParameter("preprocessimgpass" + (i + 1)));
				}
				recipe.setProces(processList[i]);
				ProcessDao.processinsert(recipe);
			}

			response.sendRedirect("MypageServlet");
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}


		private String getFileName(Part part) {
			String name = null;
			for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
				if (dispotion.trim().startsWith("filename")) {
					name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
					name = name.substring(name.lastIndexOf("\\") + 1);
					break;
				}
			}
			return name;
		}

}
