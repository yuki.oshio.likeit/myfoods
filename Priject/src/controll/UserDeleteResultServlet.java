package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/UserDeleteResultServlet")
public class UserDeleteResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserDeleteResultServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		session.removeAttribute("userList");
		session.removeAttribute("loginUser");
		session.removeAttribute("user");
		session.removeAttribute("msg");
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//userdeleteresult.jsp画面表示
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdeleteresult.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdeleteresult.jsp");
		dispatcher.forward(request, response);

	}

}
