package controll;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Recipe;
import beans.User;
import dao.FavoriteDao;
import dao.GoodgreatDao;
import dao.RecipeinsertDao;
import dao.UserDao;

@WebServlet("/GoodButtonServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-0CS8D75.000.001\\Documents\\myfoods\\Priject\\WebContent\\img", maxFileSize = 5242880)

public class GoodButtonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GoodButtonServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//レシピIDを取得。トップページでallaylistに入れ込んでStringになったidを受け取り、intにparseIntでキャスト
			String preid = request.getParameter("recipeid");
			int id = Integer.parseInt(preid);
			//ユーザー情報取得し、もしあったら、レシピ詳細画面にはおいしかったボタンとてもおいしかったボタン、お気に入りボタンを表示させるための判定用の値が入った変数を用意。
			User loginUser = (User) session.getAttribute("loginUser");
			if (loginUser != null) {
				int userid = 0;
				userid = loginUser.getId();//ユーザー固有のid


				request.setAttribute("loginUser", loginUser);

				//goodを押したときの処理
				boolean isGood = Boolean.valueOf(request.getParameter("isGood"));
				//isGoodに値がなかったら、つまりボタンをまだ押していなかったら
				if (!isGood) {
					int good = 1;
					GoodgreatDao goods = new GoodgreatDao();
					goods.goodinsert(id, userid, good);

				//値があったら、既にボタンを押していたら
				} else {
					boolean good = true;
					GoodgreatDao goods = new GoodgreatDao();
					goods.gooddelete(id, userid, good);
				}
			}


			response.sendRedirect("FoodDetailServlet?id=" + id);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//sessionからuser情報取得
			User loginUser = (User) session.getAttribute("loginUser");
			String name = loginUser.getLoginId();//ユーザーログインid

			//お気に入りに登録したいレシピid とお気に入りしたuserのidを取得
			int recipeid = Integer.parseInt(request.getParameter("recipeid"));
			int userid = loginUser.getId();//ユーザー固有のid

			//お気に入りのボタン既に押したかどうか
			FavoriteDao favorite = new FavoriteDao();
			boolean isFavorite = favorite.findfavorits(userid, recipeid);
			request.setAttribute("isFavorite", isFavorite);

			//greatを押したときの処理
			boolean isGreat = Boolean.valueOf(request.getParameter("isGreat"));
			//isGreatに値がなかったら、つまりボタンをまだ押していなかったら
			if (!isGreat) {
				int great = 1;
				GoodgreatDao isgreat = new GoodgreatDao();
				isgreat.greatinsert(recipeid, userid, great);
				//値があったら、既にボタンを押していたら
			} else {
				boolean great = true;
				GoodgreatDao isgreat = new GoodgreatDao();
				isgreat.greatdelete(recipeid, userid, great);
			}

			//recipeinsertDaoのおいしかった　とてもおいしかったボタンを表示
			GoodgreatDao appeargoodgreatbuttons = new GoodgreatDao();
			boolean itisgood = appeargoodgreatbuttons.findgood(userid, recipeid);
			boolean itisgreat = appeargoodgreatbuttons.findgreat(userid, recipeid);

			request.setAttribute("isGreat", itisgreat);

			request.setAttribute("isGood", itisgood);
			//以下非ログインユーザー用の表示画面(上記の3つのボタンを表示させない。)

			//トップページでallaylistに入れ込んでStringになったidを受け取り、intにparseIntでキャスト
			String preid = request.getParameter("recipeid");
			int id = Integer.parseInt(preid);
			//idを基に検索、料理の詳細を表示
			//使うdaoインスタンス作
			RecipeinsertDao recipe = new RecipeinsertDao();
			Recipe recipeList = recipe.outputrecipeinsert(id);
			UserDao user = new UserDao();
			String nicname = user.searchnicnamebyuserid(recipeList.getUsersid());

			request.setAttribute("recipedetail", recipeList);
			request.setAttribute("usernicname", nicname);


			//goodとgreatの集計数を表示
			int goodsum=-1;
			int greatsum=-1;
			GoodgreatDao syuukei = new GoodgreatDao();
			goodsum=syuukei.goodsum(id);
			greatsum=syuukei.greatsum(id);

			//daoメソッドで取得したカウントをそれぞれJSPへ
			request.setAttribute("goodsum", goodsum);
			request.setAttribute("greatsum", greatsum);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fooddetail.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
