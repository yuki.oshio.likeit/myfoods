package controll;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Recipe;
import dao.RecipeinsertDao;

@WebServlet("/FoodListServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-0CS8D75.000.001\\Documents\\myfoods\\Priject\\WebContent\\img", maxFileSize = 5242880)
public class FoodListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FoodListServlet() {
		super();
	}

	final static int PAGE_MAX_ITEM_COUNT = 5;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			//キーワード,日付取得
			String keyword = (String) request.getParameter("keyword");
			String createDate = (String) request.getParameter("createDate");
			String createDate2 = (String) request.getParameter("createDate2");
			//keyword　name　に文字が入っていたら

				//表示ページ番号 未指定の場合 1ページ目を表示　ページ番号をgetparameterで取得し、それがnullなら取得した番号のうちの1番目と設定
				int pageNum = Integer
						.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

				//Recipeインスタンス用意
				Recipe recipebeans = new Recipe();
				//ArrayList<Recipe>型インスタンス用意
				List<Recipe> recipeList = new ArrayList<Recipe>();
				//RecipeinsertDaoインスタンス用意
				RecipeinsertDao searchrecipeList = new RecipeinsertDao();
				//RecipeinsertDaoメソッド実行


				//keyword、createdate createdate2の検索メソッド　同時にページング処理
				recipeList = searchrecipeList.searchfoods(keyword, createDate, createDate2,pageNum,PAGE_MAX_ITEM_COUNT);


				// 検索ワードに対しての総ページ数を取得
				double itemCount = RecipeinsertDao.searchfoodsCount(keyword, createDate, createDate2);
				int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

				//総アイテム数
				request.setAttribute("itemCount", (int) itemCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);
				request.setAttribute("recipeList", recipeList);
				System.out.println("true");
				session.setAttribute("keyword",keyword);
				session.setAttribute("createDate",createDate);
				session.setAttribute("createDate2",createDate2);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/foodlist.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		//1.文字コード設定
		request.setCharacterEncoding("UTF-8");
		try {

			response.sendRedirect("FoodListServlet");
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
