package controll;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Recipe;
import dao.FavoriteDao;
import dao.MaterialDao;
import dao.ProcessDao;
import dao.RecipeinsertDao;
import dao.RecipetagDao;

@WebServlet("/FoodDeleteServlet")
public class FoodDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FoodDeleteServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
		//前の画面のrecipeidを取得
		//レシピid取得
		String preid = request.getParameter("id");
		int recipeid=Integer.parseInt(preid);

		RecipeinsertDao recipe=new RecipeinsertDao();
		Recipe recipeList= recipe.outputrecipeinsert(recipeid);

		request.setAttribute("recipe",recipeList);
		request.setAttribute("id",recipeid);

		//userdelete.jsp画面表示
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fooddelete.jsp");
		dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		//文字化け対策
		request.setCharacterEncoding("UTF-8");


		HttpSession session = request.getSession();
		//前の画面のrecipeidを取得
		//レシピid取得
		String preid = request.getParameter("id");
		int recipeid=Integer.parseInt(preid);

		//sql処理
		RecipeinsertDao recipeinsert=new RecipeinsertDao();
		recipeinsert.recipeinsertdelete(recipeid);

		MaterialDao material=new MaterialDao();
		material.materialdelete(recipeid);

		ProcessDao process=new ProcessDao();
		process.processdelete(recipeid);

		RecipetagDao tag=new RecipetagDao();
		tag.recipetagdelete(recipeid);

		FavoriteDao favorite=new FavoriteDao();
		favorite.favoritdelete(recipeid);


		//リダイレクト
		response.sendRedirect("FoodDeleteResultServlet");
	}

}
