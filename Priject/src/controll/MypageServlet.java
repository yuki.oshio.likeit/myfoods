package controll;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Recipe;
import beans.User;
import dao.RecipeinsertDao;

@WebServlet("/MypageServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-0CS8D75.000.001\\Documents\\myfoods\\Priject\\WebContent\\img", maxFileSize = 5242880)

public class MypageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MypageServlet() {
		super();

	}
	//1ページに表示する商品数を設定。固定で変更したくないのでstatic且つfinal
	final static int PAGE_MAX_ITEM = 3;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session=request.getSession();
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");
		try {

			session.getAttribute("loginUser");
			User loginUser = (User) session.getAttribute("loginUser");
			int userid=loginUser.getId();
			//daoインスタンス用意
			RecipeinsertDao searchrecipeList=new RecipeinsertDao();

			// 料理、材料、タグ,等の名前と登録日で検索の文字
			String keyword=request.getParameter("keyword");
			String createDate=request.getParameter("createDate");
			String createDate2=request.getParameter("createDate2");
			//自分がつくったレシピリストの格納先を用意
			List<Recipe> myfoodList=new ArrayList<Recipe>();
			//自分がお気に入りしたレシピリストの格納先を用意
			List<Recipe> myfavoriteList=new ArrayList<Recipe>();

			//ページング処理
			//表示ページ番号 未指定の場合 1ページ目を表示　ページ番号をgetparameterで取得し、それがnullなら取得した番号のうちの1番目と設定する文。intで設定するためparseIntでキャスト
			int myrecipepageNum = Integer.parseInt(request.getParameter("myrecipepage_num") == null ? "1" : request.getParameter("myrecipepage_num"));
			//daoクラスにあるメソッドを実行、自分で作ったレシピのインスタンスに入れ込み。↓の6変数を引数に、sqlの入ったメソッドを実行し。検索に当てはまったレシピ情報の入ったインスタンスを格納。と同時にページ形式にするためページ分のデータ取り出し
			myfoodList = searchrecipeList.searchfoodsinmypage(userid,keyword,createDate,createDate2,myrecipepageNum,PAGE_MAX_ITEM);
			// 上で取得した自分で作ったデータ数に対しての総ページ数を取得
			double myprecipeCount = searchrecipeList.findmyrecipeCount(userid,keyword,createDate,createDate2);
			int myrecipepageMax = (int) Math.ceil(myprecipeCount / PAGE_MAX_ITEM);	//↑でSELECTで取り出したデータ数/設定した表示する数で　ページの数を算出

			//自分で作ったレシピの
			//総アイテム数をJSPへ
			request.setAttribute("myprecipeCount", (int) myprecipeCount);
			// 総ページ数をJSPへ
			request.setAttribute("myrecipepageMax", myrecipepageMax);
			// 表示ページをjspへ
			request.setAttribute("myrecipepageNum", myrecipepageNum);
			// リクエストスコープに自分が作ったレシピ一覧情報をセット
			request.setAttribute("fooddetail", myfoodList);



			//以下お気に入りの処理
			//ページング処理
			//表示ページ番号 未指定の場合 1ページ目を表示　ページ番号をgetparameterで取得し、それがnullなら取得した番号のうちの1番目と設定する文。intで設定するためparseIntでキャスト
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
			//daoクラスにあるメソッドを実行、インスタンスに入れ込み。お気に入りした方↓の6変数を引数に、sqlの入ったメソッドを実行し。検索に当てはまったレシピ情報の入ったインスタンスを格納。と同時にページ形式にするためページ分のデータ取り出し
			myfavoriteList = searchrecipeList.searchfavoritefoodsinmypage(userid,keyword,createDate,createDate2,pageNum,PAGE_MAX_ITEM);
			// 上で取得したお気に入りしたデータ数に対しての総ページ数を取得
			double itemCount = searchrecipeList.findfavoriteCount(userid,keyword,createDate,createDate2);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM);	//↑でSELECTで取り出したデータ数/設定した表示する数で　ページの数を算出

			//お気に入りしたデータのページング記述
			//総アイテム数をJSPへ
			request.setAttribute("itemCount", (int) itemCount);
			// 総ページ数をJSPへ
			request.setAttribute("pageMax", pageMax);
			// 表示ページをjspへ
			request.setAttribute("pageNum", pageNum);
			// リクエストスコープに自分がお気に入りしたレシピ一覧情報をセット
			request.setAttribute("savingfoodList", myfavoriteList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mypage.jsp");
		dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
