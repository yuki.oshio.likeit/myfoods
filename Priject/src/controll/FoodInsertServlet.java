package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.Material;
import beans.Processset;
import beans.Recipe;
import beans.Tag;
import beans.User;
import dao.MaterialDao;
import dao.ProcessDao;
import dao.RecipeinsertDao;
import dao.RecipetagDao;

@WebServlet("/FoodInsertServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-0CS8D75.000.001\\Documents\\myfoods\\Priject\\WebContent\\img", maxFileSize = 5242880)
public class FoodInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FoodInsertServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");

		request.getAttribute("userList");
		HttpSession session = request.getSession();
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/foodinsert.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO  未実装：検索処理全般
		HttpSession session = request.getSession();
		//1.文字コード設定
		request.setCharacterEncoding("UTF-8");
		try {
			//2.ユーザー情報をgetParameter()で入力項目取得
			User loginUser = (User) session.getAttribute("loginUser");
			String name = loginUser.getLoginId();//ユーザーログインid
			int userid = loginUser.getId();//ユーザー固有のid

			//料理投稿画面入力情報をgetParameter()で取得
			//画像を取得するためのメソッド専用のPart型の変数を作り、nameをget専用型で取得。
			Part part = request.getPart("imgpass"); //料理画像
			//↑で取得した情報をstrin型のfileNameとして扱えるように
			String fileName = this.getFileName(part);

			//String foodimgpass=request.getParameter("imgpass");
			String foodname = request.getParameter("foodname"); //料理名
			String time = request.getParameter("time"); //所要時間
			String cost = request.getParameter("cost"); //費用
			String cal = request.getParameter("cal"); //カロリー
			String comment = request.getParameter("comment"); //コメント
			String servings = request.getParameter("servings"); //人前

			//タグの取得
			//タグはJSPのnameではname1~12で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。12個までのタグ数制限設定なので配列は12で設定。
			//もし入力欄に1つ飛ばされて入力された場合の処理をif文で分けて、引っかかったらcontinueで飛ばしてforの処理を引っかかったところだけ飛ばして処理する。
			String[] tagList = new String[12];
			for (int i = 0; i < tagList.length; i++) {
				tagList[i] = request.getParameter("tag" + (i + 1));
			}
			session.setAttribute("tagList", tagList);

			//材料名の取得
			//タグはJSPのnameではmaterialname1~12で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。12個までのタグ数制限設定なので配列は12で設定。
			String[] materialList = new String[12];

			for (int i = 0; i < materialList.length; i++) {
				materialList[i] = request.getParameter("materialname" + (i + 1));
			}

			session.setAttribute("materialList", materialList);

			//分量の取得
			//分量はJSPのnameではquantity1~12で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。12個までのタグ数制限設定なので配列は12で設定。
			String[] quantityList = new String[12];
			for (int i = 0; i < quantityList.length; i++) {
				quantityList[i] = request.getParameter("quantity" + (i + 1));
			}
			session.setAttribute("quantityList", quantityList);

			//参考画像の取得
			//参考画像はJSPのnameではname1~7で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。7個までの設定なので配列は7で設定。


			//調理手順の取得。
			//調理手順はJSPのnameではname1~7で、sevletファイルで用意した変数でforを回してカウントして対応するnameにし、値を取得する
			//tagを入力値をforで取得。7個までの設定なので配列は7で設定。
			String[] processList = new String[7];
			for (int i = 0; i < processList.length; i++) {
				processList[i] = request.getParameter("process" + (i + 1));
			}
			session.setAttribute("processList", processList);

			Recipe toukou = new Recipe();
			part.write(fileName);	//画像を書き込む為のpart宣言
			toukou.setFoodimgpass(fileName);	//Recipeインスタンスtoukouにファイル名をセット
			toukou.setUsersid(userid);
			toukou.setFoodname(foodname);
			toukou.setTime(time);
			toukou.setCost(cost);
			toukou.setCal(cal);
			toukou.setComments(comment);
			toukou.setServings(servings);


			//入力されたタグを登録
			int recipeid = RecipeinsertDao.foodinsert(toukou);

			// 固定された番号recipeIdidがforで可変した値と一緒にinsertでそれぞれ一緒に入る。
			for (int i = 0; i < tagList.length; i++) {
				//空文字を入れたくないのでcontinueで弾く
				if(tagList[i].equals("")){
					continue;
				}
				Tag recipe = new Tag();
				recipe.setRecipeid(recipeid);
				recipe.setTagname(tagList[i]);
				RecipetagDao.taginsert(recipe);
			}

			for (int i = 0; i < materialList.length; i++) {
				//空文字を入れたくないのでcontinueで弾く
				if(materialList[i].equals("")){
					continue;
				}
				Material recipe = new Material();
				recipe.setRecipeid(recipeid);
				recipe.setMaterial(materialList[i]);
				recipe.setQuantity(quantityList[i]);
				MaterialDao.materialinsert(recipe);
			}

			Part[] processimgpart = new Part[7];
			//空文字を入れたくないのでcontinueで弾く
			for (int i = 0; i < processList.length; i++) {
				if(processList[i].equals("")){
					continue;
				}
				Processset recipe = new Processset();
				recipe.setRecipeid(recipeid);
				processimgpart[i] = request.getPart("processimgpass" + (i + 1));
				if (processimgpart[i].getSize() > 0) {
					processimgpart[i].write(this.getFileName(processimgpart[i]));
					recipe.setProcessimgpass(this.getFileName(processimgpart[i]));
					recipe.setProcessnumber(i+1);
					recipe.setProces(processList[i]);
					ProcessDao.processinsert(recipe);

				}else {
				recipe.setProces(processList[i]);
				recipe.setProcessnumber(i+1);
				ProcessDao.processinsert(recipe);
				}
			}

			response.sendRedirect("FoodCreateResultServlet");
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
