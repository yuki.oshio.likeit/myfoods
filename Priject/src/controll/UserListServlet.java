package controll;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user=(User)session.getAttribute("loginUser");

		if(user==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();
		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList",userList);
		session.removeAttribute("Msg");
		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usersearch.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		UserDao userDao = new UserDao();
		// ユーザー名検索の文字
		String searchloginId=request.getParameter("loginId");
		String searchname=request.getParameter("nicname");
		String searchMail=request.getParameter("mail");
		String searchCreatedate1=request.getParameter("createDate");
		String searchCreatedate2=request.getParameter("createDate2");
		List<User> userList =userDao.namesearch(searchloginId,searchname,searchMail,searchCreatedate1,searchCreatedate2);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/usersearch.jsp");
		dispatcher.forward(request, response);

	}

}
