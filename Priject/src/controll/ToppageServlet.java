package controll;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Recipe;
import dao.RecipeinsertDao;

@WebServlet("/ToppageServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-0CS8D75.000.001\\Documents\\myfoods\\Priject\\WebContent\\img", maxFileSize = 5242880)

public class ToppageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ToppageServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//投稿日順で投稿された料理を12個表示
			ArrayList<Recipe> recipeList= RecipeinsertDao.sortbycreatedate(12);

			request.setAttribute("foodList", recipeList);




		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/toppage.jsp");
		dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}


 protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		//トップページでの検索機能
	 HttpSession session=request.getSession();
	 request.setCharacterEncoding("UTF-8");
		try {


		// 料理、材料、タグ,等の名前と登録日で検索の文字
		String keyword=request.getParameter("keyword");
		String createDate=request.getParameter("createDate");
		String createDate2=request.getParameter("createDate2");


		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("keyword",keyword);
		request.setAttribute("createDate",createDate);
		request.setAttribute("createDate2",createDate2);

		response.sendRedirect("FoodListServlet");
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
