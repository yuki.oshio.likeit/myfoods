package controll;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//sessionのloginUserインスタンス取得
		User loginUser = (User) session.getAttribute("loginUser");

		//findall()で検索し入れ込んであるデータをこのサーブレットのセッションにセット
		UserDao userDao = new UserDao();
		User user = userDao.findrow(loginUser.getLoginId());

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		//userdetailを表示させる
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		// TODO Auto-generated method stub
		//userupdate.jsp画面の更新ボタンが押されたらこのdopostメソッドで処理をする
		//入力された値を取得  あとsql文で検索条件や、更新条件に使う要となる前のJSP画面のからもらってきた自動採番されたidも取得
		String password = request.getParameter("password");
		String passwordkakunin = request.getParameter("passwordkakunin");
		String name = request.getParameter("nicname");
		String mail = request.getParameter("mail");
		String id = request.getParameter("id");

		//第1フィルター
		// 入力チェック	パスワード以外の項目が空で、パスワード確認のどちらかが違ったら更新失敗のエラーメッセージを出して
		//returnでserdetailに遷移させる処理までして下に処理をさせず中断させるif文で引っ掛ける
		if (name.equals("") || mail.equals("") || !password.equals(passwordkakunin)) {
			// リクエストスコープにエラーメッセージをセット
			session.setAttribute("Msg", "空の入力項目があるか、パスワードが違います。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//第2フィルター
		// 更新　パスワードがどちらも空だった場合の処理、と、そうでなかった場合の処理、で引っ掛ける。
		UserDao userDao = new UserDao();
		if (password.equals("")) {
			userDao.update(name, mail, id);
			session.setAttribute("Msg", "情報が更新されました。変更されているか確認してください。");
			response.sendRedirect("UserDetailServlet");
		} else {
			userDao.updatep(password, name, mail, id);
			session.setAttribute("Msg", "パスワードを含む情報が更新されました。変更されているか確認してください。");
			response.sendRedirect("UserDetailServlet");
		}
	}

}
