package controll;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Recipe;
import beans.User;
import dao.FavoriteDao;
import dao.GoodgreatDao;
import dao.RecipeinsertDao;
import dao.UserDao;

@WebServlet("/FoodDetailServlet")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-0CS8D75.000.001\\Documents\\myfoods\\Priject\\WebContent\\img", maxFileSize = 5242880)

public class FoodDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FoodDetailServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策utf-8
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//ユーザー情報取得し、もしあったら、レシピ詳細画面にはおいしかったボタンとてもおいしかったボタン、お気に入りボタンを表示させるための判定用の値が入った変数を用意。
			User loginUser = (User) session.getAttribute("loginUser");
			if (loginUser != null) {
				String name = loginUser.getLoginId();//ユーザーログインid
				int userid = 0;
				userid = loginUser.getId();//ユーザー固有のid

				//レシピIDを取得。トップページでallaylistに入れ込んでStringになったidを受け取り、intにparseIntでキャスト
				String preid = request.getParameter("id");
				int id = Integer.parseInt(preid);
				request.setAttribute("loginUser", loginUser);

				//お気に入りボタン表示管理

				//お気に入りボタンの判定用どちらのボタンを表示させるか0ならお気に入りそれより大きければ解除ボタン。
				FavoriteDao favorite = new FavoriteDao();
				boolean isFavorite = favorite.findfavorits(userid, id);
				//お気に入りボタン出現判定用の変数をJSPへ
				request.setAttribute("isFavorite", isFavorite);

				//recipeinsertDaoのおいしかった　とてもおいしかったボタンを表示
				GoodgreatDao appeargoodgreatbuttons = new GoodgreatDao();
				boolean isgood = appeargoodgreatbuttons.findgood(userid, id);
				boolean isgreat = appeargoodgreatbuttons.findgreat(userid, id);

				//お気に入りボタン出現判定用の変数をJSPへ
				request.setAttribute("isGood", isgood);
				request.setAttribute("isGreat", isgreat);
			}

			//以下日ログインユーザー用の表示画面(上記の3つのボタンを表示させない。)

			//トップページから遷移した時のレシピid取得。トップページでallaylistに入れ込んでStringになったidを受け取り、intにparseIntでキャスト
			String preid = request.getParameter("id");
			int id = Integer.parseInt(preid);

			//idを基に検索、料理の詳細を表示
			//使うdaoインスタンス作成
			RecipeinsertDao recipe = new RecipeinsertDao();
			Recipe recipeList = recipe.outputrecipeinsert(id);
			UserDao user = new UserDao();
			String nicname = user.searchnicnamebyuserid(recipeList.getUsersid());

			request.setAttribute("recipedetail", recipeList);
			request.setAttribute("usernicname", nicname);

			//goodとgreatの集計数を表示
			int goodsum=0;
			int greatsum=0;
			GoodgreatDao syuukei = new GoodgreatDao();
			goodsum=syuukei.goodsum(id);
			greatsum=syuukei.greatsum(id);

			//daoメソッドで取得したカウントをそれぞれJSPへ
			request.setAttribute("goodsum", goodsum);
			request.setAttribute("greatsum", greatsum);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fooddetail.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//sessionからuser情報取得
			User loginUser = (User) session.getAttribute("loginUser");
			String name = loginUser.getLoginId();//ユーザーログインid

			//お気に入りに登録したいレシピid とお気に入りしたuserのidを取得
			int recipeid = Integer.parseInt(request.getParameter("recipeid"));
			int userid = loginUser.getId();//ユーザー固有のid

			//jsp画面のsyorihantei nameの値取得
			boolean isFavorite = Boolean.valueOf(request.getParameter("syorihantei"));

			//お気に入りボタンが押された時の処理
			//まだ押してなかった場合
			if (!isFavorite) {
				FavoriteDao favorite = new FavoriteDao();
				favorite.favoritsinsert(recipeid, userid);
				//既に押されていた場合、
			} else {
				FavoriteDao favorite = new FavoriteDao();
				favorite.deletefavorit(recipeid, userid);

			}

			request.setAttribute("isFavorite", isFavorite);
			//↓ここでgetパラメーターに値を渡す事ができる。getでredirectする際応用できる。
			response.sendRedirect("FoodDetailServlet?id=" + recipeid);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
