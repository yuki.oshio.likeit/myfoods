package beans;

import java.io.Serializable;
import java.util.List;

import dao.MaterialDao;
import dao.ProcessDao;
import dao.RecipetagDao;


public class Recipe implements Serializable {
	private int id; //foodinsertベーステーブルの
	private String foodimgpass; //投稿料理の画像パス
	private String foodname; //料理名
	private String time; //所要時間
	private String cost; //パスワード
	private String cal; //カロリー
	private String comments; //コメント
	private String servings; //人前
	private String createDate; //投稿日時
	private String updateDate; //更新日時
	private int good = 0; //おいしかったボタンカウント
	private int great = 0; //とてもおいしかったボタンカウント
	private int usersid; //ユーザーテーブルの固有自動採番id

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFoodimgpass() {
		return foodimgpass;
	}

	public void setFoodimgpass(String foodimgpass) {
		this.foodimgpass = foodimgpass;
	}

	public String getFoodname() {
		return foodname;
	}

	public void setFoodname(String foodname) {
		this.foodname = foodname;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getCal() {
		return cal;
	}

	public void setCal(String cal) {
		this.cal = cal;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getServings() {
		return servings;
	}

	public void setServings(String servings) {
		this.servings = servings;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public int getGood() {
		return good;
	}

	public void setGood(int good) {
		this.good = good;
	}

	public int getGreat() {
		return great;
	}

	public void setGreat(int great) {
		this.great = great;
	}

	public int getUsersid() {
		return usersid;
	}

	public void setUsersid(int usersid) {
		this.usersid = usersid;
	}

//	 以下結合テーブル情報　↓タグテーブル。foreachのitemsの名前を設定するところでこのgetTagListで設定すればRecipetagDaoで入れたインスタンスの中身がここにある為、それらが順番に出力される。

	public List<Tag> getTagList() {
		return RecipetagDao.getTagListByRecipeId(this.id);
	}

	// 以下↑の文と仕組みが一緒↓材料テーブル。foreachのvarの名前でこのbeansで設定したlist名にすればMaterialDaoで入れたインスタンスの中身がここにある為、それらが順番に出力される。
	public List<Material> getMaterialList() {
		return MaterialDao.getMaterialListByRecipeId(this.id);
	}

	// 以下結合テーブル情報　↓調理手順テーブル。foreachのvarの名前でこのbeansで設定したlist名にすればProcessDaoで入れたインスタンスの中身がここにある為、それらが順番に出力される。
	public List<Processset> getProcessList() {
		return ProcessDao.getProcessListByRecipeId(this.id);
	}



}
