package beans;

import java.io.Serializable;


public class Processset implements Serializable {
	private int 	id;				//materialテーブルの自動採番ID
	private int 	recipeid;		//recipeinsertテーブルの自動採番のid
	private int 	processnumber;	//手順番号
	private String 	proces;			//手順
	private String 	processimgpass;	//参考画像パス

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRecipeid() {
		return recipeid;
	}
	public void setRecipeid(int recipeid) {
		this.recipeid = recipeid;
	}
	public int getProcessnumber() {
		return processnumber;
	}
	public void setProcessnumber(int processnumber) {
		this.processnumber = processnumber;
	}
	public String getProces() {
		return proces;
	}
	public void setProces(String proces) {
		this.proces = proces;
	}
	public String getProcessimgpass() {
		return processimgpass;
	}
	public void setProcessimgpass(String processimgpass) {
		this.processimgpass = processimgpass;
	}



}