package beans;

import java.io.Serializable;


public class User implements Serializable {
	private int id;				//ユーザーデータベーステーブルの自動採番ID
	private String loginId;		//ログインid
	private String nicname;		//ニックネーム
	private String mail;		//メールアドレス
	private String password;	//パスワード
	private String createDate;	//登録日時
	private String updateDate;	//更新日時

	// searchIdメソッド（入力されたloginIdの文字列で既に元のデータベースに存在するかを検索するメソッド）で入力された引数として使うためのコンストラクタ。JSP→servletで検索→データベースと参照するため、servletファイルのgetParameter()とJSPでの入力欄のname属性名と同一名にしている。
	public  User(int id) {
		this.id =id;

	}
	// ログイン画面のデータを保存するためのコンストラクタsearchIdメソッドで使うためのコンストラクタ
	public  User(String loginId, String nicname) {
		this.loginId = loginId;
		this.nicname = nicname;
	}

	// 他画面からの全ての入力データをここにセットするためのコンストラクタ(findAll用)
	public User(int id, String loginId, String nicname, String mail, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.nicname = nicname;
		this.mail = mail;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	//新規登録画面で入力されたデータを入れ込むメソッドを使うためのコンストラクタ(useraddメソッド用)引数をJSP、servlet、dao、元のデータベースのカラム名全て順番を揃えている。ここのUser.javaクラスを絡めたメソッドの引数名については、dao、sevletファイル全てのクラスで変数名を揃えている。
	public User( String loginId, String nicname,String password,String mail) {
		this.loginId = loginId;
		this.nicname = nicname;
		this.password = password;
		this.mail = mail;
	}
	//userdetailでのfindrowメソッド用
	public User( String loginId, String nicname, String mail,String createDate,String updateDate) {

		this.loginId = loginId;
		this.nicname = nicname;
		this.mail = mail;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	//UPDATE用
	public User(String password, String nicname, String mail) {
		this.password = password;
		this.nicname = nicname;
		this.mail =mail;
	}

	//ユーザー情報詳細用
	public User(int id,String loginId, String nicname, String mail) {
		this.id=id;
		this.loginId = loginId;
		this.nicname = nicname;
		this.mail =mail;
	}

	public User(String loginId, String nicname, String mail, int id,String createDate,
			String updateDate) {

		this.loginId = loginId;
		this.nicname = nicname;
		this.mail = mail;
		this.id=id;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getNicname() {
		return nicname;
	}
	public void setNicname(String nicname) {
		this.nicname = nicname;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}





}
