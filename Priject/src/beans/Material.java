package beans;

import java.io.Serializable;


public class Material implements Serializable {
	private int 	id;			//materialテーブルの自動採番ID
	private int 	recipeid;	//recipeinsertテーブルの自動採番のid
	private String 	material;	//材料名
	private String 	quantity;	//量
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRecipeid() {
		return recipeid;
	}
	public void setRecipeid(int recipeid) {
		this.recipeid = recipeid;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}



}