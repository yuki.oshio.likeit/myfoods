package beans;

import java.io.Serializable;


public class Tag implements Serializable {
	private int 	id;			//tagテーブルの自動採番ID
	private String 	tagname;	//タグ名
	private int 	recipeid;	//recipeinsertテーブルの自動採番のid

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}
	public int getRecipeid() {
		return recipeid;
	}
	public void setRecipeid(int recipeid) {
		this.recipeid = recipeid;
	}
}