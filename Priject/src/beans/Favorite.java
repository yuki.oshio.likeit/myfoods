package beans;

import java.io.Serializable;

public class Favorite implements Serializable {
	private int 	id;			//materialテーブルの自動採番ID
	private int 	recipeid;	//recipeinsertテーブルの自動採番のid
	private int 	usersid;	//userテーブルの自動採番
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRecipeid() {
		return recipeid;
	}
	public void setRecipeid(int recipeid) {
		this.recipeid = recipeid;
	}
	public int getUsersid() {
		return usersid;
	}
	public void setUsersid(int usersid) {
		this.usersid = usersid;
	}

}