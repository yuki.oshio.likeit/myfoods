package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import beans.User;


public class UserDao {

	//ログインし、セッションスコープにid,loginId,nicname,mailをセット。
	public User loginsessions(String loginId, String password) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			String sql = "SELECT * FROM users WHERE login_id= ? and password=?";

			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, md5(password));
			ResultSet rs = stmt.executeQuery();

			// (loginservlet等では→)ログイン失敗時の処理rsの中にloginIdとpasswordが入ってなかったらnullを返す。
			if (!rs.next()) {
				return null;
			}

			// (loginservlet等では→)ログイン成功時の処理	（↑のifで引っかかってなかったらこの処理に続くつまりlogin_idとname）
			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("nicname");
			String mail = rs.getString("mail");
			return new User(id, loginIdData, nameData, mail);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String searchId(String loginId) {
		Connection con = null;

		try {
			//1.db接続
			con = DBM.getConnection();

			String sql = "SELECT * FROM users WHERE login_id= ?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();

			// ログイン失敗時の処理rsの中にloginIdが入ってなかったらnullを返す。
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			loginId = rs.getString("login_id");

			return loginId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//loginidをidから検索するメソッド
	public String searchlogId(String id) {

		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			String sql = "SELECT login_id FROM users WHERE id= ?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginId = rs.getString("login_id");

			return loginId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//テーブル内全てのカラムのデータを取得
	public List<User> findAll() {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			con = DBM.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM users WHERE login_id  NOT IN('admin')";
			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//結果表に格納されたレコードの内容を Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String nicname = rs.getString("nicname");
				String mail = rs.getString("mail");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, nicname, mail, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//自動採番される変数idを引数にし、idでsql　SELECT文で検索。
	public User findthree(String loginid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			/*2.SQL文準備

			 */
			String sql = "SELECT login_id,nicname,mail,id,create_date,update_date FROM users WHERE login_id=?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginid);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			//

			String loginId = rs.getString("login_id");
			String nicname = rs.getString("nicname");
			String mail = rs.getString("mail");
			int id = rs.getInt("id");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			User user = new User(loginId, nicname, mail, id, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//データベースに入力した値をINSERTで入れるメソッド。
	public void useradd(String loginId, String nicname, String mail, String password) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			con = DBM.getConnection();

			// INSERT文を準備。
			String sql = "INSERT INTO users(login_id,nicname,mail,password,create_date,update_date) value(?,?,?,?,NOW(),NOW())";

			// INSERTを実行し、入力された値をいれる。
			stmt = con.prepareStatement(sql);
			// Userインスタンスに設定し、データベースに追加。
			stmt.setString(1, loginId);
			stmt.setString(2, nicname);
			stmt.setString(3, mail);
			stmt.setString(4, md5(password));

			int rs = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

	}

	//uniqueのloginidを引数にし、sql　SELECT文で検索。
	public User findrow(String loginid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			//1.db接続
			con = DBM.getConnection();

			//2.SQL文準備
			String sql = "SELECT login_id,nicname,mail,id,create_date,update_date FROM users WHERE login_id=?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginid);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			//
			String loginId = rs.getString("login_id");
			String nicname = rs.getString("nicname");
			String mail = rs.getString("mail");
			int id = rs.getInt("id");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(loginId, nicname, mail, id, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー情報削除機能
	public void userdelete(String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM users WHERE id= ?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//ユーザー情報編集(update) passwordを含む更新
	public void updatep(String password, String nicname, String mail, String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;
		try {

			//1.db接続
			con = DBM.getConnection();

			String sql = "UPDATE users SET nicname = ? , mail = ? , password = ? WHERE login_id = ?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, nicname);
			stmt.setString(2, mail);
			stmt.setString(3, md5(password));
			stmt.setString(4, id);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//上記とほぼ同じ(こちらはpasswordを含まない更新)
	public void update(String nicname, String mail, String id) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {

			//1.db接続
			con = DBM.getConnection();
			String sql = "UPDATE users SET nicname = ? , mail = ? WHERE login_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, nicname);
			stmt.setString(2, mail);
			stmt.setString(3, id);

			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//引数passwordを暗号化させるメソッド。sql文の？にメソッド名と引数で入れることができる。
	public String md5(String password) {

		//ハッシュを生成したい元の文字列 String 型のsourceを用意。そこに引数であるパスワードで初期化。このsourceにpasswordの文字列が入ることになる。
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset　文字コードを固定する。
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム　暗号化する仕組みを用意。仕組みはMD5を指定
		String algorithm = "MD5";
		//ハッシュ生成処理、byte型の配列bytesを用意と同時にnullと初期化しないといけないから同時に初期化
		byte[] bytes = null;
		try { //上で用意した配列bytesをmd5で暗号化する仕組みが入ったMessageDigest.getInstance(algorithm)を実行、「.digest(source.getBytes(fharset))」と続いているのでpasswordの文字列が入ったsourceがutf-8の文字コードでBytes型で変換されるという意味。で、変換され、暗号化された
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) { //messagedigestの文では必ずtrycatch文で囲むこと。
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} //↓のこのメソッドで上でいろいろ設定されたbytesを暗号化し、した結果をresultに入れる。
		String result = DatatypeConverter.printHexBinary(bytes);
		//resultを戻り値として返す。このメソッド名とそこに引数を入れれば　sqlの？に暗号化後のデータとして扱える。
		return result;
	}

	//UserList画面の検索に使われる文。この分で、ログインID 　ユーザー名、生年月日のいずれかを入力すれば、そのいずれかの条件一つで検索するためのメソッド。
	public List<User> namesearch(String searchloginId, String searchnicname, String searchMail,
			String searchCreatedate1, String searchCreatedate2) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			con = DBM.getConnection();
			Statement stmt = con.createStatement();


			String sql = "SELECT * FROM users WHERE login_id  NOT IN('admin')";
			//↑ここ NOT IN('admin')で、カラム名login_idの中でadmin以外のデータを検索という命令文にできる。
			//このように書くことで、検索されたデータが下にあるadd　userlistでlogin＿idがadminを含む行以外の　行のデータが格納される。

			/*このif文で、それぞれの項目で入力されたデータがサーブレットのgetparameterで取得、変数に入り、このメソッドの引数として扱われ、
			 *空文字か否かで↑で用意したsql文に追加される文が↓のif文の条件次第で対応した入力項目での部分一致で条件検索させるための文が追加され、完成する。
			 *
			 */
			//↓ログインidが空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (!searchloginId.equals("")) {
				sql += " AND login_id Like '" + "%" + searchloginId + "%'";
			}

			//↓ユーザー名が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (!searchnicname.equals("")) {
				sql += " AND nicname Like '" + "%" + searchnicname + "%'";
			}

			//↓mailが空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (!searchMail.equals("")) {
				sql += " AND mail Like '" + "%" + searchMail + "%'";
			}

			//↓登録日1（のここまでの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (!searchCreatedate1.equals("")) {
				sql += " AND createdate <= '" + searchCreatedate1 + "'";
			}

			//↓登録日2（のここまでの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (!searchCreatedate2.equals("")) {
				sql += " AND createdate >= '" + searchCreatedate2 + "'";
			}

			// SELECTを実行し、結果表を取得
			ResultSet rs = stmt.executeQuery(sql);


			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String nicname = rs.getString("nicname");
				String mail = rs.getString("mail");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(loginId, nicname, mail, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public String searchnicnamebyuserid(int usersid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;
		try {
			//1.db接続
			con = DBM.getConnection();
			String sql = "SELECT nicname FROM users WHERE id=?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, usersid);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				String nicname = rs.getString("nicname");
				return nicname;
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}
