package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Material;

public class MaterialDao {
	public static void materialinsert(Material recipe) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			stmt = con.prepareStatement("INSERT INTO material(recipe,material,quantity) value(?,?,?)");
			stmt.setInt(1, recipe.getRecipeid());
			stmt.setString(2, recipe.getMaterial());
			stmt.setString(3, recipe.getQuantity());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// TODO recipeidを引数にして材料名を繰り返しbeansインスタンスに入れ込んで、返すメソッド
	public static List<Material> getMaterialListByRecipeId(int resipeId) {
		List<Material> materialList = new ArrayList<Material>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM  material WHERE recipe=?";
			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, resipeId);
			ResultSet rs = stmt.executeQuery();
			// 格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				Material material = new Material();
				material.setId(rs.getInt("id"));
				material.setMaterial(rs.getString("material"));
				material.setQuantity(rs.getString("Quantity"));
				materialList.add(material);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return materialList;
	}

	// TODO recipeidを引数にして材料名を繰り返しbeansインスタンスに入れ込んで、返すメソッド
	public List<Material> setMaterialListByRecipeId(int recipeId) {
		List<Material> materialList = new ArrayList<Material>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM  material WHERE recipe=?";
			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeId);
			ResultSet rs = stmt.executeQuery();
			// 格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				Material material = new Material();
				material.setId(rs.getInt("id"));
				material.setMaterial(rs.getString("material"));
				material.setQuantity(rs.getString("Quantity"));
				materialList.add(material);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return materialList;
	}

	// TODO recipeidを引数にして材料名を繰り返しbeansインスタンスに入れ込んで、返すメソッド
	public List<Material> searchMaterialListByRecipeId(int resipeId) {
		List<Material> materialList = new ArrayList<Material>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM  material WHERE recipe=?";
			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, resipeId);
			ResultSet rs = stmt.executeQuery();
			// 格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			if (rs.next()) {
				Material material = new Material();
				material.setId(rs.getInt("id"));
				material.setMaterial(rs.getString("material"));
				material.setQuantity(rs.getString("Quantity"));
				materialList.add(material);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return materialList;
	}

	//material情報削除機能
	public void materialdelete(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM material WHERE recipe= ?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}


	//material情報削除機能
	public static void materialdeleteup(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM material WHERE recipe= ?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//主なrecipe情報入力した値をデータベースにINSERTで入れるメソッド。(recipeinsertテーブルに登録するデータのみ)
	public void mk2materialinsert(Material material) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;

		//↑ー1にしているのはif (rs.next())のところで確実に引っ掛けるためで、すり抜け防止のため。
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// INSERT文を準備。
			stmt = con.prepareStatement(
					"INSERT INTO material(recipe,material,quantity) value(?,?,?)");
			stmt.setInt(1, material.getRecipeid());
			stmt.setString(2, material.getMaterial());
			stmt.setString(3, material.getQuantity());

			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {

			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
