package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GoodgreatDao {
	//goodを入れ込み
	public void goodinsert(int recipeid,int usersid,int good)throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			con = DBM.getConnection();
			// INSERT文を準備。
			stmt = con.prepareStatement("INSERT INTO goodgreat(recipe,usersid,good) value(?,?,?)");
			stmt.setInt(1,recipeid);
			stmt.setInt(2,usersid);
			stmt.setInt(3, good);

			stmt.executeUpdate();


		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//greatを入れ込み
	public void greatinsert(int recipeid,int usersid,int great)throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			con = DBM.getConnection();
			// INSERT文を準備。
			stmt = con.prepareStatement("INSERT INTO goodgreat(recipe,usersid,great) value(?,?,?)");
			stmt.setInt(1,recipeid);
			stmt.setInt(2,usersid);
			stmt.setInt(3, great);
			stmt.executeUpdate();



		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//good削除機能
	public void gooddelete(int recipeid,int usersid,boolean good) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM goodgreat WHERE recipe= ? AND usersid=? AND good=?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			stmt.setInt(2, usersid);
			stmt.setBoolean(3, good);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//great削除機能
	public void greatdelete(int recipeid,int usersid,boolean great) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM goodgreat WHERE recipe= ? AND usersid=? AND great=?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			stmt.setInt(2, usersid);
			stmt.setBoolean(3, great);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}


	//goodがあるかsql　SELECT文で検索。
	public  boolean findgood(int usersid ,int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;


		try {
			//1.db接続
			con = DBM.getConnection();

			//2.SQL文準備
			String sql = "SELECT good FROM goodgreat WHERE  usersid=? AND recipe=? AND good = 1";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, usersid);
			stmt.setInt(2, recipeid);

			ResultSet rs = stmt.executeQuery();


			return rs.next();


		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//greatがあるか、sql　SELECT文で検索。
	public  boolean findgreat(int usersid ,int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;


		try {
			//1.db接続
			con = DBM.getConnection();

			//2.SQL文準備
			String sql = "SELECT great FROM goodgreat WHERE  usersid=? AND recipe=? AND great = 1";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, usersid);
			stmt.setInt(2, recipeid);

			ResultSet rs = stmt.executeQuery();


			return rs.next();


		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}


	//good集計
	//goodがあるかsql　SELECT文で検索。
	public  int goodsum(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;
		int goodsum=-1;

		try {
			//1.db接続
			con = DBM.getConnection();

			//2.SQL文準備
			String sql = "SELECT COUNT(good) AS good FROM goodgreat WHERE recipe=?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setInt(1, recipeid);

			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				goodsum=rs.getInt("good");

			}



		} catch (SQLException e) {
			e.printStackTrace();


		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return goodsum;
	}


	//great集計
	//greatがあるかsql　SELECT文で検索。
	public  int greatsum(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;
		int greatsum=-1;

		try {
			//1.db接続
			con = DBM.getConnection();

			//2.SQL文準備
			String sql = "SELECT  COUNT(great) AS great FROM goodgreat WHERE recipe=?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setInt(1, recipeid);

			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				greatsum=rs.getInt("great");

			}



		} catch (SQLException e) {
			e.printStackTrace();


		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return greatsum;
	}

}
