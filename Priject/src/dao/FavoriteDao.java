package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FavoriteDao {

	public void favoritsinsert(int recipeid,int usersid)throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			con = DBM.getConnection();
			// INSERT文を準備。
			stmt = con.prepareStatement("INSERT INTO favorite(recipe,usersid) value(?,?)");
			stmt.setInt(1,recipeid);
			stmt.setInt(2,usersid);
			stmt.executeUpdate();

			stmt.executeUpdate();;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	//favorite情報削除機能FoodDeleteServlet版
	public void favoritdelete(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM favorite WHERE recipe= ?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);

			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//favorite情報削除機能(FoodDetailServlet版)
	public void deletefavorit(int recipeid,int usersid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM favorite WHERE recipe= ? AND usersid=?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			stmt.setInt(2, usersid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}


	//uniqueのloginidを引数にし、sql　SELECT文で検索。
	public  boolean findfavorits(int usersid ,int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;


		try {
			//1.db接続
			con = DBM.getConnection();

			//2.SQL文準備
			String sql = "SELECT * FROM favorite WHERE  usersid=? AND recipe=?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, usersid);
			stmt.setInt(2, recipeid);
			ResultSet rs = stmt.executeQuery();


			return rs.next();


		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}



}
