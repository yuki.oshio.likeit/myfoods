package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Recipe;
import beans.Tag;

public class RecipetagDao {
	//投稿画面で入力したタグをデータベースへ登録
	public static void taginsert(Tag recipe) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			stmt = con.prepareStatement("INSERT INTO recipetag(recipe,tagname) value(?,?)");
			stmt.setInt(1, recipe.getRecipeid());
			stmt.setString(2, recipe.getTagname());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// TODO recipeidを引数にしてタグ名を12個以内の個数分繰り返しRecipebeansにあるtagListインスタンスに入れ込んで、返すメソッド
	public static List<Tag> getTagListByRecipeId(int resipeId) {
		List<Tag> tagList = new ArrayList<Tag>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM  recipetag WHERE recipe=?";
			// SELECTを実行し、取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, resipeId);
			ResultSet rs = stmt.executeQuery();
			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、追加
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getInt("id"));
				tag.setTagname(rs.getString("tagname"));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return tagList;
	}

	// TODO recipeidを引数にしてタグ名を12個以内の個数分繰り返しRecipebeansにあるtagListインスタンスに入れ込んで、返すメソッド
	public  List<Tag> setTagListByRecipeId(int resipeId) {
		List<Tag> tagList = new ArrayList<Tag>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM  recipetag WHERE recipe=?";
			// SELECTを実行し、取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, resipeId);
			ResultSet rs = stmt.executeQuery();
			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、追加
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getInt("id"));
				tag.setTagname(rs.getString("tagname"));
				tagList.add(tag);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return tagList;
	}

	//recipetag情報削除機能
	public void recipetagdelete(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM recipetag WHERE recipe= ?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//recipetag情報削除機能
	public static void recipetagdeleteup(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM recipetag WHERE recipe= ?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public static ArrayList<Recipe> findAllfoods(int recipeid) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBM.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM JOIN  ON recipeinsert.id = .recipe"
							+ " WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, recipeid);
			ResultSet rs = st.executeQuery();
			ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
			while (rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setId(rs.getInt("id"));
				recipe.setFoodname(rs.getString("name"));
				recipe.setFoodimgpass(rs.getString("price"));
				recipeList.add(recipe);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}
