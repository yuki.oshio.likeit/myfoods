package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Processset;

public class ProcessDao {

	public static void processinsert(Processset recipe) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			stmt = con.prepareStatement("INSERT INTO process(recipe,processimgpass,process,processnumber) value(?,?,?,?)");
			stmt.setInt(1, recipe.getRecipeid());
			stmt.setString(2, recipe.getProcessimgpass());
			stmt.setString(3, recipe.getProces());
			stmt.setInt(4, recipe.getProcessnumber());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// TODO recipeidを引数にして調理手順を繰り返しbeansインスタンスに入れ込んで、返すメソッド
	public static List<Processset> getProcessListByRecipeId(int recipeId) {
		List<Processset> processList = new ArrayList<Processset>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM  process WHERE recipe=?";
			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeId);
			ResultSet rs = stmt.executeQuery();
			// 格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				Processset proces = new Processset();
				proces.setId(rs.getInt("id"));
				proces.setProces(rs.getString("process"));
				proces.setProcessimgpass(rs.getString("processimgpass"));
				proces.setProcessnumber(rs.getInt("processnumber"));
				processList.add(proces);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return processList;
	}

	// TODO recipeidを引数にして調理手順を繰り返しbeansインスタンスに入れ込んで、返すメソッド
	public List<Processset> setProcessListByRecipeId(int recipeId) {
		List<Processset> processList = new ArrayList<Processset>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM  process WHERE recipe=?";
			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeId);
			ResultSet rs = stmt.executeQuery();
			// 格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加

			while (rs.next()) {
				Processset proces = new Processset();
				proces.setId(rs.getInt("id"));
				proces.setProces(rs.getString("process"));
				proces.setProcessnumber(rs.getInt("processnumber"));
				proces.setProcessimgpass(rs.getString("processimgpass"));
				processList.add(proces);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return processList;
	}
	// TODO recipeidを引数にして対応する料理の調理手順をbeansインスタンスに入れ込んで、返すメソッド
	public  List<Processset> searchProcessListByRecipeId(int recipeId) {
		List<Processset> processList = new ArrayList<Processset>();
		Connection con = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM  process WHERE recipe=?";
			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeId);
			ResultSet rs = stmt.executeQuery();
			// 格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			if (rs.next()) {
				Processset proces = new Processset();
				proces.setId(rs.getInt("id"));
				proces.setProces(rs.getString("process"));
				proces.setProcessimgpass(rs.getString("processimgpass"));
				proces.setProcessnumber(rs.getInt("processnumber"));
				processList.add(proces);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return processList;
	}

	//process情報削除機能
	public void processdelete(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM process WHERE recipe= ?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}


	//process情報削除機能
	public static void processdeleteup(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM process WHERE recipe= ?";
			//SQL文実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

}