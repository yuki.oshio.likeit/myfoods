package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Recipe;

public class RecipeinsertDao {
	//主なrecipe情報入力した値をデータベースにINSERTで入れるメソッド。(recipeinsertテーブルに登録するデータのみ)
	public static int foodinsert(Recipe toukou) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		int autoIncKey = -1;
		//↑ー1にしているのはif (rs.next())のところで確実に引っ掛けるためで、すり抜け防止のため。
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// INSERT文を準備。
			stmt = con.prepareStatement(
					"INSERT INTO recipeinsert(imgpass,foodname,requiredtime,cost,cal,comments,servings,usersid,create_date,update_date) value(?,?,?,?,?,?,?,?,NOW(),NOW())",
					Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, toukou.getFoodimgpass());
			stmt.setString(2, toukou.getFoodname());
			stmt.setString(3, toukou.getTime());
			stmt.setString(4, toukou.getCost());
			stmt.setString(5, toukou.getCal());
			stmt.setString(6, toukou.getComments());
			stmt.setString(7, toukou.getServings());
			stmt.setInt(8, toukou.getUsersid());
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	//トップページで12個表示される料理呼び出し
	public static ArrayList<Recipe> sortbycreatedate(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備。
			stmt = con
					.prepareStatement("SELECT id,imgpass,foodname FROM recipeinsert ORDER BY create_date ASC LIMIT ?");
			stmt.setInt(1, limit);
			stmt.executeQuery();
			ResultSet rs = stmt.executeQuery();

			//List形式で入れ込むための大本のインスタンスArrayList型Recipeを準備
			ArrayList<Recipe> recipeList = new ArrayList<Recipe>();

			while (rs.next()) {
				//繰り返しインスタンス形式で入れ込むためにRecipebeansに入れ込むためにRecipe型のインスタンスjrを用意。
				Recipe jr = new Recipe();
				jr.setId(rs.getInt("id"));
				jr.setFoodimgpass(rs.getString("imgpass"));
				jr.setFoodname(rs.getString("foodname"));
				recipeList.add(jr);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//foodlistの検索処理メソッド。
	public ArrayList<Recipe> searchfoods(String keyword, String createdate, String createdate2,int myrecipepageNum,int PAGE_MAX_ITEM)
			throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			con = DBM.getConnection();

			int startiItemNum = (myrecipepageNum - 1) * PAGE_MAX_ITEM;

			String sql = "SELECT * FROM  recipeinsert WHERE id > 1";
			ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
			//↓keywordの欄が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。


			if(keyword !=null && !keyword.equals("")) {
				sql += " AND ( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%'))";
			}


			if(createdate !=null && !createdate.equals("")) {
				sql += " AND create_date  >=  '" + createdate + "'";
			}

			if(createdate2 !=null && !createdate2.equals("")) {
				sql += " AND create_date  <=  '" + createdate2 + "'";
			}


			sql+=" ORDER BY create_date DESC LIMIT " + startiItemNum + "," + PAGE_MAX_ITEM + "";




			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setId(rs.getInt("id"));
				recipe.setCreateDate(rs.getString("create_date"));
				recipe.setFoodname(rs.getString("foodname"));
				recipe.setFoodimgpass(rs.getString("imgpass"));



				recipeList.add(recipe);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//foodlistの検索処理メソッド。カウント版
	public static double  searchfoodsCount(String keyword, String createdate, String createdate2)
				throws SQLException {
			Connection con = null;
			PreparedStatement stmt = null;

			try {
				// データベースへ接続
				con = DBM.getConnection();
				String sql = "SELECT count(*) as cnt FROM  recipeinsert WHERE id >1";


				if(keyword !=null && !keyword.equals("")) {
					sql += " AND ( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%'))";
				}


				if(createdate !=null && !createdate.equals("")) {
					sql += " AND create_date  >=  '" + createdate + "'";
				}

				if(createdate2 !=null && !createdate2.equals("")) {
					sql += " AND create_date  <=  '" + createdate2 + "'";
				}





				//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
				System.out.println(sql);
				// SELECTを実行し、結果表を取得
				stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
				ResultSet rs = stmt.executeQuery();
				double coung = 0.0;
				while (rs.next()) {
					coung = Double.parseDouble(rs.getString("cnt"));
				}
				return coung;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}

		}

	//Recipeidを基にrecipeinertテーブルの情報のみ出力
	public Recipe outputrecipeinsert(int id) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備。
			stmt = con.prepareStatement("SELECT * FROM recipeinsert WHERE id=?");
			stmt.setInt(1, id);
			stmt.executeQuery();
			ResultSet rs = stmt.executeQuery();
			//List形式で入れ込むための大本のインスタンスArrayList型Recipeを準備
			Recipe jr = new Recipe();
			if (rs.next()) {
				//繰り返しインスタンス形式で入れ込むためにRecipebeansに入れ込むためにRecipe型のインスタンスjrを用意。
				jr.setId(rs.getInt("id"));
				jr.setFoodimgpass(rs.getString("imgpass"));
				jr.setFoodname(rs.getString("foodname"));
				jr.setTime(rs.getString("requiredtime"));
				jr.setCost(rs.getString("cost"));
				jr.setCal(rs.getString("cal"));
				jr.setComments(rs.getString("comments"));
				jr.setServings(rs.getString("servings"));
				jr.setCreateDate(rs.getString("create_date"));
				jr.setUpdateDate(rs.getString("update_date"));
				jr.setGood(rs.getInt("good"));
				jr.setGreat(rs.getInt("great"));
				jr.setUsersid(rs.getInt("usersid"));
			}
			return jr;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//recipeinertテーブルの情報のみ出力
	public ArrayList<Recipe> searchownrecipe(int userid,int myrecipepageNum,int PAGE_MAX_ITEM) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
		try {
			// データベースへ接続
			con = DBM.getConnection();

			int startiItemNum = (myrecipepageNum - 1) * PAGE_MAX_ITEM;
			// SELECT文を準備。
			stmt = con.prepareStatement("SELECT * FROM recipeinsert WHERE usersid=? ORDER BY create_date DESC LIMIT ?,?");
			stmt.setInt(1, userid);
			stmt.setInt(2, startiItemNum);
			stmt.setInt(3, PAGE_MAX_ITEM);
			stmt.executeQuery();
			ResultSet rs = stmt.executeQuery();

			//List形式で入れ込むための大本のインスタンスArrayList型Recipeを準備

			while (rs.next()) {
				//繰り返しインスタンス形式で入れ込むためにRecipebeansに入れ込むためにRecipe型のインスタンスjrを用意。
				Recipe jr = new Recipe();
				jr.setId(rs.getInt("id"));
				jr.setFoodimgpass(rs.getString("imgpass"));
				jr.setFoodname(rs.getString("foodname"));
				jr.setTime(rs.getString("requiredtime"));
				jr.setCost(rs.getString("cost"));
				jr.setCal(rs.getString("cal"));
				jr.setComments(rs.getString("comments"));
				jr.setServings(rs.getString("servings"));
				jr.setCreateDate(rs.getString("create_date"));
				jr.setUpdateDate(rs.getString("update_date"));
				jr.setGood(rs.getInt("good"));
				jr.setGreat(rs.getInt("great"));
				jr.setUsersid(rs.getInt("usersid"));
				recipeList.add(jr);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	//レシピ全表示
	//recipeinertテーブルの情報のみ出力
	public ArrayList<Recipe> allrecipe(int myrecipepageNum,int PAGE_MAX_ITEM) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
		try {
			// データベースへ接続
			con = DBM.getConnection();

			int startiItemNum = (myrecipepageNum - 1) * PAGE_MAX_ITEM;
			// SELECT文を準備。
			stmt = con.prepareStatement("SELECT * FROM recipeinsert ORDER BY create_date DESC LIMIT ?,?");

			stmt.setInt(1, startiItemNum);
			stmt.setInt(2, PAGE_MAX_ITEM);
			stmt.executeQuery();
			ResultSet rs = stmt.executeQuery();

			//List形式で入れ込むための大本のインスタンスArrayList型Recipeを準備

			while (rs.next()) {
				//繰り返しインスタンス形式で入れ込むためにRecipebeansに入れ込むためにRecipe型のインスタンスjrを用意。
				Recipe jr = new Recipe();
				jr.setId(rs.getInt("id"));
				jr.setFoodimgpass(rs.getString("imgpass"));
				jr.setFoodname(rs.getString("foodname"));
				jr.setTime(rs.getString("requiredtime"));
				jr.setCost(rs.getString("cost"));
				jr.setCal(rs.getString("cal"));
				jr.setComments(rs.getString("comments"));
				jr.setServings(rs.getString("servings"));
				jr.setCreateDate(rs.getString("create_date"));
				jr.setUpdateDate(rs.getString("update_date"));
				jr.setGood(rs.getInt("good"));
				jr.setGreat(rs.getInt("great"));
				jr.setUsersid(rs.getInt("usersid"));
				recipeList.add(jr);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//レシピ全表示文のカラムカウント
	//recipeinertテーブルの情報のみ出力

	public static double allrecipeCount() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBM.getConnection();
			st = con.prepareStatement("select count(*) as cnt from recipeinsert");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//recipeinsert情報削除機能
	public void recipeinsertdelete(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM recipeinsert WHERE id= ?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//recipeinsert情報削除機能
	public static void foodinsertdeleteup(int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		try {

			//1.db接続
			con = DBM.getConnection();

			//前の画面のサーブレットから持ってきたidで検索し、そのidの行を削除
			String sql = "DELETE FROM recipeinsert WHERE id= ?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, recipeid);
			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public static void recipeinsertupdate(Recipe toukou) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;

		//データベースを一々切るためのtrycatch文を作成
		try {

			//1.db接続
			con = DBM.getConnection();
			String sql = "UPDATE recipeinsert SET imgpass=?, foodname = ? ,requiredtime = ?,cost = ? ,cal = ?,comments = ? ,servings = ? WHERE id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, toukou.getFoodimgpass());
			stmt.setString(2, toukou.getFoodname());
			stmt.setString(3, toukou.getTime());
			stmt.setString(4, toukou.getCost());
			stmt.setString(5, toukou.getCal());
			stmt.setString(6, toukou.getComments());
			stmt.setString(7, toukou.getServings());
			stmt.setInt(8, toukou.getId());

			int rs = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


	//検索機能ワード、日付で表示される料理呼び出し
	public  ArrayList<Recipe> searchbykeyword(String keyword) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBM.getConnection();
			// SELECT文を準備。
			stmt = con
					.prepareStatement("SELECT id,imgpass,foodname FROM recipeinsert ORDER BY create_date ASC LIMIT ?");
			stmt.setString(1, keyword);
			stmt.executeQuery();
			ResultSet rs = stmt.executeQuery();

			//List形式で入れ込むための大本のインスタンスArrayList型Recipeを準備
			ArrayList<Recipe> recipeList = new ArrayList<Recipe>();

			while (rs.next()) {
				//繰り返しインスタンス形式で入れ込むためにRecipebeansに入れ込むためにRecipe型のインスタンスjrを用意。
				Recipe jr = new Recipe();
				jr.setId(rs.getInt("id"));
				jr.setFoodimgpass(rs.getString("imgpass"));
				jr.setFoodname(rs.getString("foodname"));
				recipeList.add(jr);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	//ワード、日付で表示されるマイページ内で自分の料理呼び出し
	public ArrayList<Recipe> searchfoodsinmypage(int usersid,String keyword, String createdate, String createdate2,int myrecipepageNum,int PAGE_MAX_ITEM)
			throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {

			// データベースへ接続
			con = DBM.getConnection();

			int startiItemNum = (myrecipepageNum - 1) * PAGE_MAX_ITEM;

			String sql = "SELECT * FROM  recipeinsert WHERE usersid = '" + usersid + "'";
			ArrayList<Recipe> myfoodList = new ArrayList<Recipe>();
			//↓keywordの欄が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
/*			if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}else if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  usersid = '" + usersid + "'";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate.equals("")) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%'or id in(select recipe from material where material Like '" + "%" + keyword + "%'))or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND  usersid = '" + usersid +"'";
			}

			//↓登録日（のここからの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "'AND  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}else if(keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "' AND  usersid = '" + usersid + "'";
			}else if (keyword==null||keyword.equals("")&&!createdate2.equals("")&&createdate2!=null) {
				sql += " WHERE  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}

			sql+=" ORDER BY create_date DESC LIMIT " + startiItemNum + "," + PAGE_MAX_ITEM + "";*/

			if(keyword !=null && !keyword.equals("")) {
				sql += " AND ( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%'))";
			}


			if(createdate !=null && !createdate.equals("")) {
				sql += " AND create_date  >=  '" + createdate + "'";
			}

			if(createdate2 !=null && !createdate2.equals("")) {
				sql += " AND create_date  <=  '" + createdate2 + "'";
			}


			sql+=" ORDER BY create_date DESC LIMIT " + startiItemNum + "," + PAGE_MAX_ITEM + "";


			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setId(rs.getInt("id"));
				recipe.setCreateDate(rs.getString("create_date"));
				recipe.setFoodname(rs.getString("foodname"));
				recipe.setFoodimgpass(rs.getString("imgpass"));


				myfoodList.add(recipe);
			}
			return myfoodList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//ワード、日付で表示されるマイページ内で自分の料理呼び出し,のカラムカウント版
	public static double findmyrecipeCount(int usersid,String keyword, String createdate, String createdate2)
			throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {

			// データベースへ接続
			con = DBM.getConnection();



			String sql = "SELECT COUNT(*) AS cnt FROM  recipeinsert WHERE usersid = '" + usersid + "'";
			ArrayList<Recipe> myfoodList = new ArrayList<Recipe>();
			//↓keywordの欄が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
/*			if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}else if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  usersid = '" + usersid + "'";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate.equals("")) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%'or id in(select recipe from material where material Like '" + "%" + keyword + "%'))or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND  usersid = '" + usersid +"'";
			}

			//↓登録日（のここからの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "'AND  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}else if(keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "' AND  usersid = '" + usersid + "'";
			}else if (keyword==null||keyword.equals("")&&!createdate2.equals("")&&createdate2!=null) {
				sql += " WHERE  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "'";
			}*/

			if(keyword !=null && !keyword.equals("")) {
				sql += " AND ( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%'))";
			}


			if(createdate !=null && !createdate.equals("")) {
				sql += " AND create_date  >=  '" + createdate + "'";
			}

			if(createdate2 !=null && !createdate2.equals("")) {
				sql += " AND create_date  <=  '" + createdate2 + "'";
			}



			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
			ResultSet rs = stmt.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//おいしかったボタンを表示させるかどうかの判定
	public  boolean findgoods(int usersid ,int recipeid) {
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;


		try {
			//1.db接続
			con = DBM.getConnection();

			//2.SQL文準備
			String sql = "SELECT * FROM recipeinsert WHERE  usersid=? AND recipe=?";
			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, usersid);
			stmt.setInt(2, recipeid);
			ResultSet rs = stmt.executeQuery();


			return rs.next();


		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}



	//お気に入りしたレシピをmypageで表示させる
	public  ArrayList<Recipe> findfavoritsbyuserid(int usersid,int pageNum,int PAGE_MAX_ITEM) throws SQLException{
		//data baseを接続、作成するためのConnection conを作成
		Connection con = null;
		ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
		PreparedStatement stmt = null;
		try {
			//1.db接続
			con = DBM.getConnection();

			int startiItemNum = (pageNum - 1) * PAGE_MAX_ITEM;
			//2.SQL文準備
			String sql = "SELECT * FROM recipeinsert JOIN favorite ON recipeinsert.id = favorite.recipe WHERE  favorite.usersid = ? ORDER BY create_date DESC LIMIT ?,?";

			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。

			//SQL文実行及び、値を入れ込む予定のカラム値(左からカウント)と入れ込んだ値を扱うために必要なkey名を設定し、setResurlt方の箱に実行結果を格納。
			stmt.setInt(1, usersid);
			stmt.setInt(2, startiItemNum);
			stmt.setInt(3, PAGE_MAX_ITEM);
			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setId(rs.getInt("id"));
				recipe.setCreateDate(rs.getString("create_date"));
				recipe.setFoodname(rs.getString("foodname"));
				recipe.setFoodimgpass(rs.getString("imgpass"));


				recipeList.add(recipe);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}




	//お気に入りしたレシピをmypageで検索ワード、日付を基に表示させる
	public ArrayList<Recipe> searchfavoritefoodsinmypage(int usersid,String keyword, String createdate, String createdate2,int pageNum,int PAGE_MAX_ITEM)
			throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {

			// データベースへ接続
			con = DBM.getConnection();
			int startiItemNum = (pageNum - 1) * PAGE_MAX_ITEM;

			String sql = "SELECT * FROM  recipeinsert WHERE id in(SELECT recipe FROM favorite WHERE usersid='" + usersid + "')";
			ArrayList<Recipe> myfoodList = new ArrayList<Recipe>();
			//↓keywordの欄が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
/*			if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') ";
			}else if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "')";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "')";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate.equals("")) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%'or id in(select recipe from material where material Like '" + "%" + keyword + "%'))or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid +"'";
			}

			//↓登録日（のここからの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "'AND  create_date <= '" + createdate2 + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}else if(keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}else if (keyword==null||keyword.equals("")&&!createdate2.equals("")&&createdate2!=null) {
				sql += " WHERE  create_date <= '" + createdate2 + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}

			sql+=" ORDER BY create_date DESC LIMIT " + startiItemNum + "," + PAGE_MAX_ITEM + "";*/

			if(keyword !=null && !keyword.equals("")) {
				sql += " AND ( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%'))";
			System.out.println("only keyword");
			}


			if(createdate !=null && !createdate.equals("")) {
				sql += " AND create_date  >=  '" + createdate + "'";
				System.out.println("only createdate");
			}

			if(createdate2 !=null && !createdate2.equals("")) {
				sql += " AND create_date  <=  '" + createdate2 + "'";
				System.out.println("only createdate2");
			}

			sql+=" ORDER BY create_date DESC LIMIT " + startiItemNum + "," + PAGE_MAX_ITEM + "";


			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setId(rs.getInt("id"));
				recipe.setCreateDate(rs.getString("create_date"));
				recipe.setFoodname(rs.getString("foodname"));
				recipe.setFoodimgpass(rs.getString("imgpass"));


				myfoodList.add(recipe);
			}
			return myfoodList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//お気に入りしたレシピをmypageで検索ワード、日付を基に表示させるメソッドのカウント版
	public static double findfavoriteCount(int usersid,String keyword, String createdate, String createdate2)
			throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {

			// データベースへ接続
			con = DBM.getConnection();

			String sql = "SELECT count(*) AS cnt FROM  recipeinsert WHERE id in(SELECT recipe FROM favorite WHERE usersid='" + usersid + "')";
			ArrayList<Recipe> myfoodList = new ArrayList<Recipe>();
			//↓keywordの欄が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
/*			if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') ";
			}else if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "')";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "')";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate.equals("")) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%'or id in(select recipe from material where material Like '" + "%" + keyword + "%'))or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid +"'";
			}

			//↓登録日（のここからの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "'AND  create_date <= '" + createdate2 + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}else if(keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}else if (keyword==null||keyword.equals("")&&!createdate2.equals("")&&createdate2!=null) {
				sql += " WHERE  create_date <= '" + createdate2 + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}*/

			if(keyword !=null && !keyword.equals("")) {
				sql += " AND ( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%'))";
			}


			if(createdate !=null && !createdate.equals("")) {
				sql += " AND create_date  >=  '" + createdate + "'";
			}

			if(createdate2 !=null && !createdate2.equals("")) {
				sql += " AND create_date  <=  '" + createdate2 + "'";
			}


			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
			ResultSet rs = stmt.executeQuery();

			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}
	/**
	 * 自分が作ったレシピに当てはまったデータ数をカウントしカウント数を取得するためのメソッドを作成
	 */
	public  double findmyrecipeCount(int usersid) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBM.getConnection();
			st = con.prepareStatement("SELECT COUNT(*) AS cnt FROM recipeinsert WHERE usersid=?");
			st.setInt(1,usersid);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}







	//お気に入りしたレシピ検索し当てはまったもののカウント
	//お気に入りしたレシピをmypageで検索ワード、日付を基に表示させる
	public ArrayList<Recipe> searchfavoritefoodsinmypageCount(int usersid,String keyword, String createdate, String createdate2)
			throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {

			// データベースへ接続
			con = DBM.getConnection();


			String sql = "SELECT count(*) AS cnt FROM  recipeinsert WHERE id>1";
			ArrayList<Recipe> myfoodList = new ArrayList<Recipe>();
			//↓keywordの欄が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
/*			if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "')";
			}else if (!keyword.equals("")&&keyword!=null&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date  >=  '" + createdate + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "')";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select recipe from material where material Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND create_date <= '" + createdate2 + "' AND  usersid = '" + usersid + "' AND id in(select recipe from favorite where usersid= '" + usersid + "')";
			}else if(!keyword.equals("")&&keyword!=null&&!createdate.equals("")) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%'or id in(select recipe from material where material Like '" + "%" + keyword + "%'))or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid +"'";
			}

			//↓登録日（のここからの方）が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if (keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null&&!createdate2.equals("")&&createdate2!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "'AND  create_date <= '" + createdate2 + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}else if(keyword==null||keyword.equals("")&&!createdate.equals("")&&createdate!=null) {
				sql += "WHERE create_date  >=  '" + createdate + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}else if (keyword==null||keyword.equals("")&&!createdate2.equals("")&&createdate2!=null) {
				sql += " WHERE  create_date <= '" + createdate2 + "' AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}
*/
			if(keyword !=null && !keyword.equals("")) {
				sql += " AND ( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%'))AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}


			if(createdate !=null && !createdate.equals("")) {
				sql += " AND create_date  >=  '" + createdate + "'AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}

			if(createdate2 !=null && !createdate2.equals("")) {
				sql += " AND create_date  <=  '" + createdate2 + "'AND id in(select recipe from favorite where usersid= '" + usersid + "') AND  usersid = '" + usersid + "'";
			}






			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setId(rs.getInt("id"));
				recipe.setCreateDate(rs.getString("create_date"));
				recipe.setFoodname(rs.getString("foodname"));
				recipe.setFoodimgpass(rs.getString("imgpass"));


				myfoodList.add(recipe);
			}
			return myfoodList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}







	//トップページからカテゴリクリックのキーワードで検索
	//トップページやナビゲーションバーでの検索
	public ArrayList<Recipe> searchfoodsbykeyword(String keyword,int myrecipepageNum,int PAGE_MAX_ITEM)
			throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			con = DBM.getConnection();

			int startiItemNum = (myrecipepageNum - 1) * PAGE_MAX_ITEM;

			String sql = "SELECT * FROM  recipeinsert ";
			ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
			//↓keywordの欄が空文字じゃなかったらその入力された文字列に部分的に一致するかの文を文字結合する。
			if(keyword!=null) {
				sql += "WHERE( id in(select recipe from recipetag where tagname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where foodname Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where requiredtime  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cost  Like '" + "%" + keyword + "%')or id in(select id from recipeinsert where cal  Like '" + "%" + keyword + "%')) ";
			sql+=" ORDER BY create_date DESC LIMIT '" + startiItemNum + "','" + PAGE_MAX_ITEM + "'";

			}
			//↓実行されたsql文をコンソールに表示させ、バグ改修に使用する。
			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			stmt = con.prepareStatement(sql);	//←PreparedStatement型のstmtの内容を必ず入れる事。入れないで↓のexecuteQuery で実行してしまうとnullになってしまう。
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setId(rs.getInt("id"));
				recipe.setCreateDate(rs.getString("create_date"));
				recipe.setFoodname(rs.getString("foodname"));
				recipe.setFoodimgpass(rs.getString("imgpass"));



				recipeList.add(recipe);
			}
			return recipeList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}
}