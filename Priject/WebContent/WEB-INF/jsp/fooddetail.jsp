<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="wrapper">
		<div class="maxwidth80">
			<div class="container-fluid">
				<div class="white">
					<div class="row py-1">
						<div class="col-sm-6" align="left">
							<h2>${recipedetail.foodname}</h2>
						</div>
						<div class="col-sm-6">
							<ul class="row">
								<li class="col"><h4>費用:${recipedetail.cost}</h4></li>
								<li class="col"><h4>所要時間:${recipedetail.time}</h4></li>
								<li class="col"><h4>cal:${recipedetail.cal}</h4></li>
							</ul>
						</div>
					</div>

					<div class="row">
						<img src="img/${recipedetail.foodimgpass}" id=""
							name="newpiclavel" alt="写真" width="700" height="500">
						<div class="col">
							<div>${recipedetail.comments}</div>
							<p></p>
							<div class="float-right py-3 row">投稿者:${usernicname}</div>
							<div>
								<p></p>
							</div>
							<div class=" mt-5 row">
							<c:if test="${loginUser.loginId!=null}">

							<c:if test="${isGood}">
							<form action="GoodButtonServlet" method="get">
							<div class="col-sm-3">
							<input type="hidden" value="${isGood}" name="isGood">
							<input type="hidden" value="${recipedetail.id}" name="recipeid">
								<button class="btn btn-secondary">美味(評価を解除)</button>
							</div>
							</form>
							</c:if>
							<c:if test="${!isGood}">
							<form action="GoodButtonServlet" method="get">
							<div class="col-sm-3">
							<input type="hidden" value="${isGood}" name="isGood">
							<input type="hidden" value="${recipedetail.id}" name="recipeid">
								<button class="btn btn-success">おいしかった</button>
							</div>
							</form>
							</c:if>

							<c:if test="${isGreat}">
							<form action="GoodButtonServlet" method="post">
							<div class="col-sm-2">
							<input type="hidden" value="${isGreat}" name="isGreat">
							<input type="hidden" value="${recipedetail.id}" name="recipeid">
								<button class="btn btn-secondary">超美味(評価を解除)</button>
							</div>
							</form>
							</c:if>
							<c:if test="${!isGreat}">
							<form action="GoodButtonServlet" method="post">
							<div class="col-sm-2">
							<input type="hidden" value="${isGreat}" name="isGreat">
							<input type="hidden" value="${recipedetail.id}" name="recipeid">
								<button class="btn btn-danger">!!!!!とてもおいしかった!!!!!</button>
							</div>
							</form>
							</c:if>
							</c:if>
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-sm-4 text-success font-weight-bold" align="right">
							おいしかったと思った人 "${goodsum}"人
					</div>
					<div class="col-sm-4 text-danger font-weight-bold" align="right">
							とてもおいしかったと思った人 "${greatsum}"人
					</div>
					</div>
					<div class="row ">
					<div class="col-sm-12" align="center"><h5>T a g s</h5></div>
					</div>
					<div class="m-1 row">
						<!-- tag -->
						<!--↓Recipebeansのインスタンスを参照させて、そのインスタンスが入っているbeansを参照させて、その性質を利用してgetTagList()を参照させている。-->
						<c:forEach var="tag" items="${recipedetail.tagList}">
							<a class="col-1"> ${tag.tagname} </a>
						</c:forEach>
						<!--↑参照させたgetTagList()に入っているメソッドで入れ込んだ変数名を指定すること。 -->
					</div>

					<div class="row">
						<div class="col">
							<div class="font-weight-light font-italic" align="right">投稿日:${recipedetail.createDate}</div>
						</div>
					</div>
				</div>
				<h1 class="">使用材料　　　　　　　　　　　　　　調理手順</h1>
				<div class="row">
					<!--使用材料-->
					<div class="col-sm-6 listbox white">
						<ul>
							<li class="py-2 text-right" >${recipedetail.servings}人前当たりの量</li>
							<c:forEach var="material" items="${recipedetail.materialList}">
								<li><strong>${material.material}</strong><em>${material.quantity}</em>
									|</li>
							</c:forEach>
						</ul>
					</div>
					<!--調理手順-->
					<div class="col-sm-6  white">
						<ul>
							<li>
								<div class="row py-1">
								<c:forEach var="process" items="${recipedetail.processList}">
									<div class="col-sm-1 py-1" align="left">
										${process.processnumber}
									</div>
									<div class="col-sm-3 py-1" align="left">
									<img src="img/${process.processimgpass}"  alt="写真"width="120" height="85" align="left">
									</div>
									<div class="col-sm-8 py-1" align="left">
									${process.proces}
									</div>

								</c:forEach>
								</div>
							</li>
						</ul>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6  white text-right">

					</div>

					<div class="col-sm-6  white ">
						<div class="row">
							<div class="col-sm-3" align="left">
								<button class="btn btn-primary" onclick="window.print();">印刷する</button>
							</div>
							<c:if test="${loginUser.loginId!=null}">

							<c:if test="${isFavorite}">
							<form action="FoodDetailServlet" method="post">
							<div class="col-sm-3 ">
							<input type="hidden" value="${isFavorite}" name="syorihantei">
							<input type="hidden" value="${recipedetail.id}" name="recipeid">
								<button class="btn btn-secondary">お気に入り済(もう一度押してこのレシピをお気に入り解除する)</button>
							</div>
							</form>
							</c:if>
							<c:if test="${!isFavorite}">
							<form action="FoodDetailServlet" method="post">
							<div class="col-sm-3 ">
							<input type="hidden" value="${isFavorite}" name="syorihantei">
							<input type="hidden" value="${recipedetail.id}" name="recipeid">
								<button class="btn btn-success">このレシピをお気に入りに保存する</button>
							</div>
							</form>
							</c:if>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>

	</div>
</body>

</html>
