<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h1 class="mtb30px">ユーザー情報削除確認</h1>
		<div class="white">
			<p class="mtb30px">
				<font size="5">${loginId}さんのアカウントを削除します。</font>
			</p>
			<p class="mtb30px">元には戻せません。本当に削除してよろしいでしょうか。</p>
			<div class="row">
				<div align="left">
					<a href="javascript: history.back()"
						class="btn btn-outline-secondary">戻る</a>
				</div>
				<div class="col-sm-6"></div>
				<div class="col-sm-3 buttons">
					<form action="UserDeleteServlet" method="post">
						<input type="hidden" value="${id}" name="id"></input> <input
							type="hidden" value="${loginId}" name="id">
						<button type="submit" value="検索"
							class="btn btn-outline-success my-2 my-sm-0">OK</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<p class="m-1">(c)copy right</p>
	</footer>

</body>
</html>