<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="wrapper">
		<div class="container">

			<h1 class="mtb30px">ユーザー一覧管理（管理者用）</h1>

			<div class="white">
				<form action="UserListServlet" method="post" class="form-horizontal">
					<div class="form-group row py-1">
						<label for="logid" class="col-sm-2 col-form-label">ログインID</label>
						<div class="col-sm-10">
							<input type="text" id="logid" name="loginId" size="30">
						</div>
					</div>
					<div class="form-group row">
						<label for="un" class="col-sm-2 col-form-label">ユーザ名</label>
						<div class="col-sm-10">
							<input type="text" id="un" name="nicname" size="30">
						</div>
					</div>
					<div class="form-group row">
						<label for="un" class="col-sm-2 col-form-label">メールアドレス</label>
						<div class="col-sm-10">
							<input type="text" id="un" name="mail" size="30">
						</div>
					</div>
					<div class="form-group row">
						<label for="bn" class="col-sm-2 col-form-label">登録日</label>
						<div class="col-sm-3">
							<input type="date" id="bn" name="createDate">
						</div>
						<div class="col-sm-2 text-center">~</div>
						<div class="col-sm-3">
							<input type="date" name="createDate2">
						</div>
					</div>
					<div align="left">
						<a href="javascript: history.back()"
							class="btn btn-outline-secondary">戻る</a>
					</div>
					<div class="text-right">
						<button type="submit" value="検索"
							class="btn btn-success my-2 my-sm-0">検索</button>
					</div>
					<div class="row">
						<c:if test="${Msg != null}">
							<div class="alert alert-danger" role="alert">${Msg}</div>
						</c:if>
					</div>

				</form>
			</div>
		</div>
		<div class="m-5">
			<table class="table">
				<thead>
					<tr class="table-warning">
						<th scope="col">ログインID</th>
						<th scope="col">ユーザー名</th>
						<th scope="col">メールアドレス</th>
						<th scope="col">登録日</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.nicname}</td>
							<td>${user.mail}</td>
							<td>${user.createDate}</td>
							<td>
								<form method="get" action="Userdetailservlet"
									class="form-horizontal">
									<a class="btn btn-primary"
										href="Userdetailservlet?id=${user.id}">詳細</a>
								</form> <c:if
									test="${loginUser.loginId == 'admin'||loginUser.loginId == user.loginId}">
									<form method="get" action="UserUpdateServlet"
										class="form-horizontal">
										<a class="btn btn-success"
											href="UserUpdateServlet?id=${user.id}">更新</a>
									</form>
								</c:if> <c:if test="${loginUser.loginId == 'admin'}">
									<form method="get" action="UserDeleteServlet"
										class="form-horizontal ">
										<a class="btn btn-danger"
											href="UserDeleteServlet?id=${user.id}">削除</a>
									</form>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="row">

			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>
	</div>
</body>

</html>
