<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="wrapper">
		<div class="container" style="width: 500px;">
			<div class="white">
				<form action="LoginServlet" method="post">
					<div class=" text-center m-xl-3 py-2">
						<h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1>
					</div>
					<div class="row -group m-xl-3">
						<label for="inputLoginId">ログインID</label> <input type="text"
							name="loginId" id="inputLoginId" class="form-control col-auto"
							placeholder="ログインIDを入力してください" autofocus>
					</div>
					<div class="row -group m-xl-3">
						<label for="inputPassword">Password</label> <input type="password"
							name="password" id="inputPassword" class="form-control mb-xl-5"
							placeholder="パスワードを入力してください">
					</div>
					<button class="btn-danger sm-3 btn-block " type="submit">ログイン</button>
				</form>
				<br> <br>
				<div class="py-1">
					<a href="javascript: history.back()"
						class="btn btn-outline-secondary btn-block my-2 my-sm-0">戻る</a>
				</div>

				<div class="py-4">
					<form action="UserCreateServlet" method="get">
						<button type="submit" value="検索"
							class="btn btn-outline-primary btn-block my-2 my-sm-0">新しくアカウントを作る</button>
					</form>
				</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>
	</div>
</body>

</html>
