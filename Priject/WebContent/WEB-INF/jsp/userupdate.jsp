<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="wrapper">
		<div class="container-fluid" align="center">
			<div class="container-large">
				<h1 class="userdetail">ユーザー情報更新</h1>
				<div class="white">
					<div class="row">
						<c:if test="${Msg != null}">
							<div class="alert alert-danger" role="alert">${Msg}</div>
						</c:if>
					</div>
					<form action="UserUpdateServlet" method="post"
						class="form-horizontal">
						<div class="form-group row">
							<label for="loginid" class="col-sm-4 col-form-label">ログインID</label>
							<div class="col-sm-8" id="loginid">
								<input type="hidden" value="${user.loginId}" name="id">
								<div class="m-1" align="left">${user.loginId}</div>
							</div>
						</div>

						<div class="form-group row">
							<label for="pass" class="col-sm-4 col-form-label">パスワード</label>
							<div class="col-sm-8" id="pass">
								<input class="col " type="password" name="password"
									pattern="^([a-zA-Z0-9]{6,})$" title="半角英数字6文字以上入力してください" maxlength='12' >
							</div>
						</div>

						<div class="form-group row">
							<label for="kakunin" class="col-sm-4 col-form-label">パスワード(確認)</label>
							<div class="col-sm-8" id="kakunin" align="left">
								<input class="col " type="password" name="passwordkakunin"
									pattern="^([a-zA-Z0-9]{6,})$" title="半角英数字6文字以上を入力してください" maxlength='12' >
							</div>
						</div>

						<div class="form-group row">
							<label for="name" class="col-sm-4 col-form-label">ニックネーム</label>
							<div class="col-lg-8" id="name" ailgn="right">
								<input maxlength="11" required class="col" type="text"
									value="${user.nicname}" name="nicname"
									placeholder="${user.nicname}" maxlength='10' >
							</div>
						</div>

						<div class="form-group row">
							<label for="mail" class="col-sm-4 col-form-label">メールアドレス</label>
							<div class="col-sm-8" id="mail" align="left">
								<input placeholder="${user.mail}" required class="col"
									value=${user.mail } type="text" name="mail"
									pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
									title="半角英数字で@以降も全て入力してください。">
							</div>
						</div>
						<div class="submitbutton">
							<button type="submit" class="btn btn-primary">更新する</button>
						</div>
					</form>

					<div align="left">
						<a href="javascript: history.back()"
							class="btn btn-outline-secondary">戻る</a>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>
	</div>
</body>

</html>
