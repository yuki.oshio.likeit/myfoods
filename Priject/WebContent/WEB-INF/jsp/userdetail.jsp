<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="wrapper">
		<div class="maxwidth80">
			<div class="container-fluid" align="center">
				<div class="container-large">
					<h1 class="userdetail">ユーザー情報</h1>
					<div class="white">
						<div class="row">
							<c:if test="${Msg != null}">
								<div class="alert alert-danger" role="alert">${Msg}</div>
							</c:if>
						</div>

						<form action="UserDetailServlet" method="post"
							class="form-horizontal">
							<input type="hidden" value="${user.loginId}" name="id"></input>
							<div class="form-group row">
								<label for="loginid" class="col-sm-5 col-form-label">ログインID</label>
								<div class="col-sm-7" id="loginid">
									<div align="left">${user.loginId}</div>
								</div>
							</div>
							<div class="form-group row">
								<label for="nicname" class="col-sm-5 col-form-label">ニックネーム</label>
								<div class="col-lg-7" id="nicname">
									<div align="left">${user.nicname}</div>
								</div>
							</div>

							<div class="form-group row">
								<label for="mail" class="col-sm-5 col-form-label">メールアドレス</label>
								<div class="col-sm-7" id="mail">
									<div align="left">${user.mail}</div>
								</div>
							</div>
							<div class="submitbutton">
								<button type="submit" class="btn btn-primary">更新画面へ</button>
							</div>
						</form>
						<div class="row">
							<div class="col-sm-7">
								<a href="javascript: history.back()"
									class="btn btn-outline-secondary">戻る</a>
							</div>
							<c:if test="${user.loginId!='admin'}">
								<div class="col-sm-5">
									<form action="UserDeleteServlet method="get">
										<a type="submit" class="btn btn-warning"
											href="UserDeleteServlet?id=${user.id}">自分のアカウントを削除する</a>
									</form>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>
	</div>
</body>

</html>
