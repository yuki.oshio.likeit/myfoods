<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h1 class="mtb30px">料理削除確認</h1>
		<div class="white">
			<p class="mtb30px">
				<font size="5">${recipe.foodname}</font>
			</p>
			<p class="mtb30px">のレシピを本当に削除してよろしいでしょうか。</p>
			<div class="row">
				<form action="MypageServlet" method="get">
					<div class="col-sm-3 buttons">
						<button type="submit" value="検索" class="btn btn-danger my-2 my-sm-0">やっぱりやめる</button>
					</div>
				</form>
				<div class="col-sm-6"></div>
				<div class="col-sm-3 buttons">
					<form action="FoodDeleteServlet" method="post">
						<input type="hidden" value="${id}" name="id">
						<button type="submit" class="btn btn-outline-success my-2 my-sm-0">OK</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<p class="m-1">(c)copy right</p>
	</footer>

</body>
</html>