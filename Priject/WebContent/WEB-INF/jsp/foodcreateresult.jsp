<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<h1 class="mtb30px">お料理登録確認</h1>
		<div class="white">
			<p class="mtb30px">
				<font size="5"></font>
			</p>
			<h4>投稿を完了しました。</h4>
			<div class="row">
				<form action="MypageServlet" method="get">
					<div class="col-sm-3 buttons">
						<button type="submit" value="検索"
							class="btn btn-danger my-2 my-sm-0">マイページへ</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<footer>
		<p class="m-1">(c)copy right</p>
	</footer>

</body>

</html>
