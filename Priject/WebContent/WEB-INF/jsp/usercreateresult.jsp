<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h1 class="mtb30px">ユーザー情報登録確認</h1>
		<div class="white">
			<p class="mtb30px">
				<font size="5">${user.nicname}さん</font>
			</p>
			<p class="mtb30px">の登録を完了しました。</p>
			<div class="row">
				<form action="MypageServlet" method="get">
					<div class="col ">
						<button type="submit" value="検索" class="btn btn-danger ">マイページへ</button>
					</div>
				</form>
				<div class="col" align="right">
					<form action="ToppageServlet" method="get">
						<button type="submit" value="検索" class="btn btn-success  ">トップページへ</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<p class="m-1">(c)copy right</p>
	</footer>

</body>

</html>
