<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="wrapper">
		<div class="maxwidth80">
			<div class="container-fluid">
				<h1>料理を更新</h1>
				<div class="white">
					<c:if test="${errMsg != null}">
						<div class="alert alert-danger" role="alert">${errMsg}</div>
					</c:if>
					<%-- ↓画像を扱うformには必ずenctypeを記入すること。--%>
					<form action="OneFoodUpdateServlet" method="post"
						enctype="multipart/form-data">
						<div class="row">
							<img class="d-block mx-auto m-2"
								src="img/${recipedetail.foodimgpass}" width="180" height="130">
								<input type="hidden" value="${recipedetail.foodimgpass}" class="bs-custom-file-input" id="customFile" name="imgpass">
						</div>
						<div class="input-group-sm py-3 row">
							<label for="custom" class="">新しい画像:</label>
							<div class="custom-file col-7" id="custom">
								<input type="file" class="bs-custom-file-input" id="customFile"	name="newimgpass">
							</div>

							<div class="input-group-append col">
								<button type="button" class="btn btn-outline-secondary reset">取消</button>
							</div>
						</div>
						<div class="row py-3">
							<label for="foodname" class="m-2">料理名※必須</label> <input
								class="col" type="text" name="foodname" id="foodname"
								placeholder="${recipedetail.foodname}"
								value="${recipedetail.foodname}">
						</div>

						<div class="row m-3">
							<div class="col-sm-4">
								所要時間 <input class="col" type="text" name="time"
									placeholder="${recipedetail.time}" value="${recipedetail.time}">
							</div>

							<div class="col-sm-4">
								費用 <input class="col" type="text" name="cost"
									placeholder="${recipedetail.cost}" value="${recipedetail.cost}">
							</div>

							<div class="col-sm-4">
								カロリー <input class="col" type="text" name="cal"
									placeholder="${recipedetail.cal}" value="${recipedetail.cal}">
							</div>
						</div>

						<div class="" align="center">
							コメント
							<input maxlength="50" class="col"size="100"
								value="${recipedetail.comments}" name="comment"
								placeholder="${recipedetail.comments}">

						</div>

						<div class="py-3">
							<div class="py-1" align="center">タグ(任意)12個まで</div>
						</div>

						<div class="row">

							<div class="col-sm-1">
								<input class="col" value="${tagList[0].tagname}" type="text"
									name="tag1" placeholder="${tagList[0].tagname}">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag2"
									placeholder="${tagList[1].tagname}"
									value="${tagList[1].tagname}">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag3"
									placeholder="${tagList[2].tagname}"
									value="${tagList[2].tagname}">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag4"
									placeholder="${tagList[3].tagname}"
									value="${tagList[3].tagname}">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag5"
									placeholder="${tagList[4].tagname}"
									value="${tagList[4].tagname}">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag6"
									placeholder="${tagList[5].tagname}"
									value="${tagList[5].tagname}">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag7"
									placeholder="${tagList[6].tagname}"
									value="${tagList[6].tagname}">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag8"
									placeholder="${tagList[7].tagname}"
									value="${tagList[7].tagname}">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag9"
									placeholder="${tagList[8].tagname}"
									value="${tagList[8].tagname}">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag10"
									placeholder="${tagList[9].tagname}"
									value="${tagList[9].tagname}">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag11"
									placeholder="${tagList[10].tagname}"
									value="${tagList[10].tagname}">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag12"
									placeholder="${tagList[11].tagname}"
									value="${tagList[11].tagname}">
							</div>
						</div>

						<div class="py-3">
							<h5 class="py-1">使用材料 分量</h5>
						</div>
						<div class="center">
							<input class="col-sm-1" type="text" name="servings"
								value="${recipedetail.servings}"
								placeholder="${recipedetail.servings}">人前
						</div>

						<br>

						<div class="row">

							<div class="col-sm-6">
								<input class="col" type="text" name="materialname1"
									value="${materialList[0].material}"
									placeholder="${materialList[0].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity1"
									placeholder="${materialList[0].quantity}"
									value="${materialList[0].quantity}">
							</div>

						</div>

						<hr>

						<br>

						<div class="row">

							<div class="col-sm-6">
								<input class="col" type="text" name="materialname2"
									value="${materialList[1].material}"
									placeholder="${materialList[1].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity2"
									placeholder="${materialList[1].quantity}"
									value="${materialList[1].quantity}">
							</div>

						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname3"
									value="${materialList[2].material}"
									placeholder="${materialList[2].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity3"
									value="${materialList[2].quantity}"
									placeholder="${materialList[2].quantity}">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname4"
									value="${materialList[3].material}"
									placeholder="${materialList[3].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity4"
									placeholder="${materialList[3].quantity}"
									value="${materialList[3].quantity}">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname5"
									placeholder="${materialList[4].material}"
									value="${materialList[4].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity5"
									placeholder="${materialList[4].quantity}"
									value="${materialList[4].quantity}">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname6"
									placeholder="${materialList[5].material}"
									value="${materialList[5].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity6"
									placeholder="${materialList[5].quantity}"
								value="${materialList[5].quantity}">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname7"
									placeholder="${materialList[6].material}"
									value="${materialList[6].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity7"
									placeholder="${materialList[6].quantity}"
								value="${materialList[6].quantity}">
							</div>
						</div>

												<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname8"
									placeholder="${materialList[7].material}"
									value="${materialList[7].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity8"
									placeholder="${materialList[7].quantity}"
								value="${materialList[7].quantity}">
							</div>
						</div>

												<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname9"
									placeholder="${materialList[8].material}"
									value="${materialList[8].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity9"
									placeholder="${materialList[8].quantity}"
								value="${materialList[8].quantity}">
							</div>
						</div>

												<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname10"
									placeholder="${materialList[9].material}"
									value="${materialList[9].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity10"
									placeholder="${materialList[9].quantity}"
								value="${materialList[9].quantity}">
							</div>
						</div>

												<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname11"
									placeholder="${materialList[10].material}"
									value="${materialList[10].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity11"
									placeholder="${materialList[10].quantity}"
								value="${materialList[10].quantity}">
							</div>
						</div>

												<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname12"
									placeholder="${materialList[11].material}"
									value="${materialList[11].material}">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity12"
									placeholder="${materialList[11].quantity}"
								value="${materialList[11].quantity}">
							</div>
						</div>

						<hr>


						<div class="py-3">
							<h5 class="py-1">作成手順</h5>
						</div>
						<div class="row">
							<h6 class="m-1">1.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file col-sm-5 m-2">
									<%--↓配列形式のデータは指定するとき必ず[0]と指定を忘れないこと。${}内はjavaの変数指定と同じ --%>
									<img src="img/${processList[0].processimgpass}" alt="写真"
										width="120" height="85" align="left"> <input type="file"
										class="bs-custom-file-input" id="customfile"
										 name="processimgpass1">
										<input type="hidden" value="${processList[0].processimgpass}" name="preprocessimgpass1">
								</div>

								<div class="input-group-append m-2 col-sm-1" align="right">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>

							<input class="col-sm-4" size="64" name="process1"
								value="${processList[0].proces}"
								placeholder="${procesList[0].proces}">


						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">2.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file col-sm-5 m-2">
									<img src="img/${processList[1].processimgpass}" alt="写真"
										width="120" height="85" align="left"> <input type="file"
										class="bs-custom-file-input" id="customfile"
										 name="processimgpass2">
										<input type="hidden" value="${processList[1].processimgpass}" name="preprocessimgpass2">
								</div>
								<div class="input-group-append m-2 col-sm-1">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<input class="col-sm-4" size="64" name="process2"
								value="${processList[1].proces}"
								placeholder="${processList[1].proces}">
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">3.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file col-sm-5 m-2">
									<img src="img/${processList[2].processimgpass}" alt="写真"
										width="120" height="85" align="left"> <input type="file"
										class="bs-custom-file-input" id="customfile"
										 name="processimgpass3">
										<input type="hidden" value="${processList[2].processimgpass}" name="preprocessimgpass3">
								</div>
								<div class="input-group-append m-2 col-sm-1">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<input class="col-sm-4" size="64" name="process3"
								value="${processList[2].proces}"
								placeholder="${processList[2].proces}">
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">4.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file col-sm-5 m-2">
									<img src="img/${processList[3].processimgpass}" alt="写真"
										width="120" height="85" align="left"> <input type="file"
										class="bs-custom-file-input" id="customfile"
										 name="processimgpass4">
										<input type="hidden" value="${processList[3].processimgpass}" name="preprocessimgpass4">
								</div>
								<div class="input-group-append m-2 col-sm-1">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<input class="col-sm-4" size="64" name="process4"
								value="${processList[3].proces}"
								placeholder="${processList[3].proces}">
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">5.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file col-sm-5 m-2">
									<img src="img/${processList[4].processimgpass}" alt="写真"
										width="120" height="85" align="left"> <input type="file"
										class="bs-custom-file-input" id="customfile"
									 name="processimgpass5">
										<input type="hidden" value="${processList[4].processimgpass}" name="preprocessimgpass5">
								</div>
								<div class="input-group-append m-2 col-sm-1">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<input class="col-sm-4" size="64" name="process5"
								value="${processList[4].proces}"
								placeholder="${processList[4].proces}">
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">6.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file col-sm-5 m-2">
									<img src="img/${processList[5].processimgpass}" alt="写真"
										width="120" height="85" align="left"> <input type="file"
										class="bs-custom-file-input" id="customfile"
									 name="processimgpass6">
								<input type="hidden" value="${processList[5].processimgpass}" name="preprocessimgpass6">
								</div>
								<div class="input-group-append m-2 col-sm-1">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<input class="col-sm-4" size="64" name="process6"
								value="${processList[5].proces}"
								placeholder="${processList[5].proces}">
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">7.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file col-sm-5 m-2">
									<img src="img/${processList[6].processimgpass}" alt="写真"
										width="120" height="85" align="left">
										<input type="file"
										class="bs-custom-file-input" id="customfile" name="processimgpass7">
										<input type="hidden" value="${processList[6].processimgpass}" name="preprocessimgpass7">
								</div>
								<div class="input-group-append m-2 col-sm-1">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<input class="col-sm-4" size="128" name="process7"
								value="${processList[6].proces}"
								placeholder="${processList[6].proces}">
						</div>
						<br>
						<hr>
						<br>
						<div align="center">
							<input type="hidden" value="${id}" name="id">
							<button class="btn btn-warning" type="submit">投稿する</button>
						</div>
					</form>
					<div class="col newreg text-left">
						<a href="javascript: history.back()"
							class="btn btn-outline-danger btn-lg">やっぱりやめる</a>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>

	</div>

</body>

</html>
