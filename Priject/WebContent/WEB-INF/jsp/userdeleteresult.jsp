<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h1 class="mtb30px">ユーザー情報削除確認</h1>
		<div class="white">
			<p class="mtb30px">削除を完了しました。</p>
			<div class="row">
				<c:if test="${loginId=='admin'}">
					<div class="col-sm-3 buttons">
						<form action="UserListServlet" method="get">
							<button type="submit" class="btn btn-success my-2 my-sm-0">ユーザーリストへ</button>
						</form>
					</div>
				</c:if>
				<div class="col-sm-3 buttons">
					<form action="ToppageServlet" method="get">
						<button type="submit" class="btn btn-danger my-2 my-sm-0">トップページへ</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<p class="m-1">(c)copy right</p>
	</footer>

</body>

</html>
