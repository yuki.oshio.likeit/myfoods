<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="wrapper">
		<div class="container">
			<h1>ユーザー新規登録</h1>
			<div class="white">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<form action="UserCreateServlet" method="post">

					<div class="m-xl-3  passtxt" align="center">
						ログインID(登録後変更不可)<input class="col" type="text" name="loginId"
							placeholder="ローマ字・数字のみ" pattern="^[0-9A-Za-z]+$"
							title="半角英数字で入力してください"required>
					</div>

					<div class="m-xl-3  passtxt" align="center">
						ニックネーム(10文字以内)<input maxlength="11" class="col" type="text"
							name="nicname" maxlength='10' required>
					</div>

					<div class="m-xl-3  passtxt" align="center">
						パスワード(6文字以上12文字以内)<input class="col" type="password"
							name="password" pattern="^([a-zA-Z0-9]{6,})$"
							title="半角英数字6文字以上入力してください" maxlength='12'  required>
					</div>

					<div class="m-xl-3  passtxt" align="center">
						パスワード(確認)<input class="col" type="password" name="passwordkakunin"
							pattern="^([a-zA-Z0-9]{6,})$" title="半角英数字6文字以上入力してください" maxlength='12'   required>
					</div>

					<div class="m-xl-3  passtxt" align="center">
						メールアドレス<input class="col" type="text" name="mail"
							placeholder="pc・携帯メールアドレスを入力"
							pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
							title="半角英数字で@以降も全て入力してください" required>
					</div>

					<div class="m-xl-3 py-3 " align="center">
						<button class="btn btn-primary" type="submit" value="登録">登録する</button>
					</div>
				</form>
				<div align="left">
					<a href="javascript: history.back()"
						class="btn btn-outline-secondary">戻る</a>
				</div>

			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>
	</div>
</body>

</html>
