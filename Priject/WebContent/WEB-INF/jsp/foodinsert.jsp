<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="wrapper">
		<div class="maxwidth80">
			<div class="container-fluid">
				<h1>料理を投稿</h1>
				<div class="white">
					<c:if test="${errMsg != null}">
						<div class="alert alert-danger" role="alert">${errMsg}</div>
					</c:if>
					<%-- ↓画像を扱うformには必ずenctypeを記入すること。--%>
					<form action="FoodInsertServlet" method="post"
						enctype="multipart/form-data">
						<div class="row">
							<img class="d-block mx-auto" src="../img/asa.jpg" width="180"
								height="130">
						</div>
						<div class="input-group-sm py-3 row">
							<label for="custom" class="">料理画像又は動画:</label>
							<div class="custom-file col-7" id="custom">
								<input type="file" class="bs-custom-file-input" id="customFile"
									name="imgpass" required>
							</div>

							<div class="input-group-append col">
								<button type="button" class="btn btn-outline-secondary reset">取消</button>
							</div>
						</div>
						<div class="row py-3">
							<label for="foodname" class="m-2">料理名※必須</label> <input
								class="col" type="text" name="foodname" id="foodname" required>
						</div>

						<div class="row m-3">
							<div class="col-sm-4">
								所要時間 <input class="col" type="text" name="time"
									placeholder="例 40分">
							</div>

							<div class="col-sm-4">
								費用 <input class="col" type="text" name="cost"
									placeholder="例 540円">
							</div>

							<div class="col-sm-4">
								カロリー <input class="col" type="text" name="cal"
									placeholder="例 1000kcal">
							</div>
						</div>

						<div class="" align="center">
							コメント
							<textarea maxlength="50" class="col" rows="4" cols="50"
								name="comment" placeholder="コメントを記入してください"></textarea>
						</div>

						<div class="py-3">
							<div class="py-1" align="center">タグ(任意)12個まで</div>
						</div>

						<div class="row">
							<div class="col-sm-1">
								<input class="col" type="text" name="tag1">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag2">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag3">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag4">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag5">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag6">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag7">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag8">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag9">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag10">
							</div>
							<div class="col-sm-1">
								<input class="col" type="text" name="tag11">
							</div>

							<div class="col-sm-1">
								<input class="col" type="text" name="tag12">
							</div>
						</div>

						<div class="py-3">
							<h3 class="py-1">使用材料　　　　　　　　　　　　　　　　　　　　　　　　分量</h3>
						</div>
						<div class="center">
							<input class="col-sm-1" type="text" name="servings"
								placeholder="1" required>人前
						</div>

						<br>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname1"
									placeholder="鰆の切り身">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity1" placeholder="1切">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname2"
									placeholder="大根">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity2"
									placeholder="3分の1本">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname3"
									placeholder="人参">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity3"
									placeholder="3分の1本">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname4">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity4">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname5">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity5">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname6">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity6">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname7">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity7">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname8">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity8">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname9">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity9">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname10">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity10">
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname11">
							</div>

							<div class="col-sm-6">
								<input class="col" type="text" name="quantity11">
							</div>
						</div>

						<hr>

						<hr>

						<div class="row">
							<div class="col-sm-6">
								<input class="col" type="text" name="materialname12">
							</div>
							<div class="col-sm-6">
								<input class="col" type="text" name="quantity12">
							</div>
						</div>
						<hr>
						<div class="py-3">
							<h5 class="py-1">作成手順</h5>
						</div>
						<div class="row">
							<h6 class="m-1">1.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file">
									<input type="file" class="bs-custom-file-input" id="customfile"
										name="processimgpass1">
								</div>
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<textarea class="col-sm-4" rows="3" cols="30" name="process1"
								placeholder="ここに手順やコツを記入してください" required></textarea>
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">2.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file">
									<input type="file" class="bs-custom-file-input" id="customfile"
										name="processimgpass2">
								</div>
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<textarea class="col-sm-4" rows="3" cols="30" name="process2"
								placeholder="ここに手順やコツを記入してください"></textarea>
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">3.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file">
									<input type="file" class="bs-custom-file-input" id="customfile"
										name="processimgpass3">
								</div>
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<textarea class="col-sm-4" rows="3" cols="30" name="process3"
								placeholder="ここに手順やコツを記入してください"></textarea>
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">4.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file">
									<input type="file" class="bs-custom-file-input" id="customfile"
										name="processimgpass4">
								</div>
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<textarea class="col-sm-4" rows="3" cols="30" name="process4"
								placeholder="ここに手順やコツを記入してください"></textarea>
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">5.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file">
									<input type="file" class="bs-custom-file-input" id="customfile"
										name="processimgpass5">
								</div>
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<textarea class="col-sm-4" rows="3" cols="30" name="process5"
								placeholder="ここに手順やコツを記入してください"></textarea>
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">6.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file">
									<input type="file" class="bs-custom-file-input" id="customfile"
										name="processimgpass6">
								</div>
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<textarea class="col-sm-4" rows="3" cols="30" name="process6"
								placeholder="ここに手順やコツを記入してください"></textarea>
						</div>
						<br>
						<hr>
						<br>
						<div class="row">
							<h6 class="m-1">7.</h6>
							<div class="input-group-sm py-2 col-sm-6">
								<h4>参考画像又は動画:</h4>
								<div class="custom-file">
									<input type="file" class="bs-custom-file-input" id="customfile"
										name="processimgpass7">
								</div>
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-secondary reset">取消</button>
								</div>
							</div>
							<textarea class="col-sm-4" rows="3" cols="30" name="process7"
								placeholder="ここに手順やコツを記入してください"></textarea>
						</div>
						<br>
						<hr>
						<br>
						<div align="center">
							<button class="btn btn-warning" type="submit">投稿する</button>
						</div>
					</form>
					<div class="col newreg text-left">
						<a href="javascript: history.back()"
							class="btn btn-outline-danger btn-lg">やっぱりやめる</a>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>

	</div>

</body>

</html>
