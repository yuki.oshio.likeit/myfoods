<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="wrapper">
		<div class="container">
			<div class="white"></div>
		</div>
		<div class="wrapper m-5">
			<div class="white maxwidth80">
				<div class="container">
					<form action="FoodListServlet" method="get"
						class="form-horizontal">
						<div class="form-group row py-1">
							<label for="un" class=" col-f3orm-label">料理名や材料等のキーワード:</label>
							<div class="col">
								<input class="m-1" type="text" id="un" name="keyword" size="30">
							</div>
						</div>
						<div class="form-group row">
							<label for="bn" class="col-form-label">投稿日</label>
							<div class="col">
								<input type="date" id="bn" name="createDate">
							</div>
							<div class="col py-1">から</div>
							<div class="col">
								<input type="date" name="createDate2">
							</div>
							<div class="col py-1">まで</div>
						</div>
						<div class="text-right">
							<button type="submit"
								class="btn btn-outline-success my-2 my-sm-0">検索</button>
						</div>
					</form>
				</div>
			</div>

			<div class="azukirow m-2">
			<h3>新しく投稿されたレシピ</h3>
			</div>
			<div class="row light m-2">

				<!-- ↓必ずダブルクォーテーション↓で囲む事 -->
				<c:forEach var="food" items="${foodList}">
					<div class="col-2 ">
						<div class="py-1">
							<a href="FoodDetailServlet?id=${food.id}"><img
								src="img/${food.foodimgpass}" width="180" height="130"></a>
						</div>
						<!-- ↑jspで画像を出力させる場合getメソッドではurlでやり取りするため相対パスでなく、フォルダ名とファイル名で指定 -->
						<div><a href="FoodDetailServlet?id=${food.id}">${food.foodname}</a></div>
					</div>
				</c:forEach>
			</div>

			<div>
				<h3>味から探す</h3>
				<div class="white row">

					<ul class="col">
						<li>
							<div>

							</div>
							<div>
								<a href="FoodListServlet?keyword=塩味">塩味</a>
							</div>
						</li>
					</ul>
					<ul class="col">
						<li>
							<div>

							</div>
							<div>
								<%--↓hrefのgetでこの様に変数を用意出来る。でサーブレットのgetでSELECT検索してという動作を実現できる --%>
								<a href="FoodListServlet?keyword=辛味">辛味</a>
							</div>
						</li>
					</ul>
					<ul class="col">
						<li>
							<div>

							</div>
							<div>
								<a href="FoodListServlet?keyword=酸味">酸味</a>

							</div>
						</li>
					</ul>
					<ul class="col">
						<li>
							<div>

							</div>
							<div>
								<a href="FoodListServlet?keyword=苦味">苦味</a>

							</div>
						</li>
					</ul>
					<ul class="col">
						<li>
							<div>

							</div>
							<div>
								<a href="FoodListServlet?keyword=甘味">甘味</a>

							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-5">
				<h3>材料から探す</h3>
				<div class="white">
					<div class="row nikurow m-3">肉</div>
					<ul>
						<li>
							<div class="row">
								<a href="FoodListServlet?keyword=豚肉" class="col">豚肉</a> <a
									href="FoodListServlet?keyword=鶏肉" class="col">鶏肉</a> <a
									href="FoodListServlet?keyword=牛肉" class="col">牛肉</a> <a
									href="FoodListServlet?keyword=羊肉" class="col">羊肉</a> <a
									href="FoodListServlet?keyword=馬肉" class="col">馬肉</a> <a
									href="FoodListServlet?keyword=鹿肉" class="col">鹿肉</a> <a
									href="FoodListServlet?keyword=猪肉" class="col">猪肉</a> <a
									href="FoodListServlet?keyword=その他　肉" class="col">その他の肉</a>
							</div>
						</li>
					</ul>
					<div class="row sakanarow m-3">海産物</div>
					<ul>
						<li>
							<div class="row">
								<a href="FoodListServlet?keyword=マグロ" class="col">マグロ</a> <a
									href="FoodListServlet?keyword=サバ" class="col">サバ</a> <a
									href="FoodListServlet?keyword=アジ" class="col">アジ</a> <a
									href="FoodListServlet?keyword=シジミ" class="col">シジミ</a>
							</div>
						</li>
					</ul>
					<div class="row yasairow m-3">野菜</div>
					<ul>
						<li>
							<div class="row">
								<a href="FoodListServlet?keyword=人参" class="col">人参</a> <a
									href="FoodListServlet?keyword=もやし" class="col">もやし</a> <a
									href="FoodListServlet?keyword=キャベツ" class="col">キャベツ</a> <a
									href="FoodListServlet?keyword=ピーマン" class="col">ピーマン</a>
							</div>
						</li>
					</ul>
					<div class="row sonotarow m-3">その他</div>
					<ul>
						<li>
							<div class="row ">
								<a href="FoodListServlet?keyword=小麦粉" class="col">小麦粉</a> <a
									href="FoodListServlet?keyword=果物" class="col">果物</a> <a
									href="FoodListServlet?keyword=米" class="col">米</a> <a
									href="FoodListServlet?keyword=その他" class="col">その他</a>
							</div>
						</li>
					</ul>
					<p></p>
				</div>
			</div>
			<div>
				<h3>その他</h3>
				<div class="white">
					<ul>
						<li>
							<div>
								<div class="row m-2">
									<a href="FoodListServlet?keyword=筋トレ" class="col">筋トレ食</a> <a
										href="FoodListServlet?keyword=アウトドア" class="col">アウトドア</a> <a
										href="FoodListServlet?keyword=時短" class="col">時短</a> <a
										href="FoodListServlet?keyword=体調不良時" class="col">体調不良時に</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>
	</div>
</body>

</html>
