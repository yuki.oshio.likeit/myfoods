<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/s.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="wrapper">
		<div class="white maxwidth80">
			<h1 class="mtb30px">マイページ</h1>
			<div class="container">
				<form action="MypageServlet" method="get" class="form-horizontal">
					<div class="form-group row py-1">
						<label for="un" class=" col-form-label">料理名や材料等のキーワード:</label>
						<div class="col">
							<input class="m-1" type="text" id="un" name="keyword" size="30" value="${keyword}">
						</div>
					</div>
					<div class="form-group row">
						<label for="bn" class="col-form-label">投稿日</label>
						<div class="col">
							<input type="date" id="bn" name="createDate" value="${createDate}">
						</div>
						<div class="col py-1">から</div>
						<div class="col">
							<input type="date" name="createDate2" value="${createDate2}">
						</div>
						<div class="col py-1">まで</div>
					</div>
					<div class="text-right">
						<button type="submit" value="検索"
							class="btn btn-outline-success my-2 my-sm-0">検索</button>
					</div>
				</form>
			</div>
		</div>
		<div class="wrapper m-1">
			<div class="maxwidth90">
				<h3>投稿した料理</h3>
				<table class="table">
					<thead>
						<tr class="table-warning">
							<th scope="col">保存日</th>
							<th scope="col">画像</th>
							<th scope="col">料理名</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="food" items="${fooddetail}">
							<tr>
								<td>${food.createDate}</td>
							<td><a href="FoodDetailServlet?id=${food.id}"><img
									src="img/${food.foodimgpass}" width="180" height="130"></a></td>
							<td><a href="FoodDetailServlet?id=${food.id}">${food.foodname}</a></td>
								<td>
									<form method="get" action="FoodDetailServlet"
										class="form-horizontal">
										<a class="btn btn-primary"
											href="FoodDetailServlet?id=${food.id}">詳細</a>
									</form>
									<form method="get" action="OneFoodUpdateServlet"
										class="form-horizontal">
										<a class="btn btn-success"
											href="OneFoodUpdateServlet?id=${food.id}">更新</a>
									</form>
									<form method="get" action="FoodDeleteServlet"
										class="form-horizontal">
										<a class="btn btn-danger"
											href="FoodDeleteServlet?id=${food.id}">削除</a>
									</form>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

				<div class="row mx-auto" >
			<ul class="pagination  justify-content-center">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${myrecipepageNum == 1}">
						<li class="disabled"><a><i class="material-icons">前のページへ</i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a href="MypageServlet?keyword=${keyword}&myrecipepage_num=${myrecipepageNum - 1}"><i class="material-icons">前のページへ</i></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(myrecipepageNum - 5) > 0 ? myrecipepageNum - 5 : 1}" end="${(myrecipepageNum + 5) > myrecipepageMax ? myrecipepageMax : myrecipepageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${myrecipepageNum == status.index }"> class="active" </c:if>><a href="MypageServlet?keyword=${keyword}&myrecipepage_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${myrecipepageNum == myrecipepageMax || myrecipepageMax == 0}">
					<li class="disabled"><a><i class="material-icons">次のページへ</i></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a href="MypageServlet?keyword=${keyword}&myrecipepage_num=${myrecipepageNum + 1}"><i class="material-icons">次のページへ</i></a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</div>
				<br> <br> <br>
				<h3>保存した料理</h3>
				<table class="table">
					<thead>
						<tr class="table-warning">
							<th scope="col">保存日</th>
							<th scope="col">画像</th>
							<th scope="col">料理名</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="save" items="${savingfoodList}">

							<tr>
								<td>${save.createDate}</td>
							<td><a href="FoodDetailServlet?id=${save.id}"><img
									src="img/${save.foodimgpass}" width="180" height="130"></a></td>
							<td><a href="FoodDetailServlet?id=${save.id}">${save.foodname}</a></td>
								<td>

									<form method="get" action="Fooddetailservlet"
										class="form-horizontal">
										<a class="btn btn-primary"
											href="FoodDetailServlet?id=${save.id}">詳細</a>
									</form>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="row mx-auto">
			<ul class="pagination  text-light justify-content-center">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a><i class="material-icons">前のページへ</i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a href="MypageServlet?keyword=${keyword}&page_num=${pageNum - 1}"><i class="material-icons">前のページへ</i></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a href="MypageServlet?keyword=${keyword}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a><i class="material-icons">次のページへ</i></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a href="MypageServlet?keyword=${keyword}&page_num=${pageNum + 1}"><i class="material-icons">次のページへ</i></a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</div>
			</div>
		</div>
		<footer>
			<p class="m-1">(c)copy right</p>
		</footer>
	</div>
</body>

</html>
