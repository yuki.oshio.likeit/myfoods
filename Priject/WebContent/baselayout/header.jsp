<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<body>

	<header class="azuki">
		<ul class="nav justify-content-end navbarp">
			<li class="nav-item mr-auto">
				<!--トップッページに戻る--> <a class="nav-link active lawhite"
				href="ToppageServlet">MyFooDs</a>
			</li>
			<c:if test="${loginUser.loginId=='admin'}">
				<li class="nav-item">
					<div class="headerLinks m-2 mr-2 ">
						<a href="UserListServlet" class="lawhite">管理者用</a>
					</div>
				</li>
			</c:if>
			<c:if test="${loginUser.loginId !=null}">
				<li class="nav-item">
					<div class="headerLinks m-2 mr-2 ">
						<a href="MypageServlet" class="lawhite">マイページ</a>
					</div>
				</li>
			</c:if>
			<c:if test="${loginUser.loginId !=null}">
				<li class="nav-item">
					<div class="headerLinks m-2 mr-2 ">
						<a href="UserDetailServlet" class="lawhite">ユーザー情報</a>
					</div>
				</li>
			</c:if>
			<c:if test="${loginUser.loginId !=null}">
				<li class="nav-item">
					<div class="headerLinks m-2 mr-2">
						<a href="FoodInsertServlet" class="lawhite">料理を投稿する</a>
					</div>
				</li>
			</c:if>
			<li class="nav-item">
				<div class="headerLinks m-2 mr-2 ">
					<a href="ToppageServlet" class="lawhite">トップページへ </a>
				</div>
			</li>
			<li class="nav-item">
				<form class="form-inline" action="FoodListServlet" method="get">
					<div class="input-group">
						<input type="text" class="form-control" name="keyword" size="50"
							placeholder="料理名、カテゴリなどを入力">
						<button type="submit" class="btn btn-outline-light mx-1">検索</button>
					</div>
				</form>
			</li>
			<c:if test="${loginUser.loginId!=null}">
				<li class="nav-item"><a class="nav-link active lawhite"
					href="UserDetailServlet">${loginUser.nicname}さん</a></li>
			</c:if>


			<c:if test="${loginUser.loginId!=null}">

				<li class="nav-item"><a class="btn btn-outline-light"
					href="LogoutServlet">ログアウト</a></li>
			</c:if>

			<c:if test="${loginUser.loginId==null}">
				<li class="nav-item"><a class="btn btn-outline-light"
					href="LoginServlet">ログイン</a></li>
			</c:if>

		</ul>
	</header>
	<div class="">
		<p></p>
	</div>

