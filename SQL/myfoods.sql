CREATE DATABASE foods DEFAULT CHARACTER SET utf8;
USE foods;

CREATE TABLE users(id SERIAL,login_id varchar(255) UNIQUE NOT NULL,
nicname varchar(255) NOT NULL,mail varchar(255) NOT NULL,password varchar(255) NOT NULL,
create_date DATETIME NOT NULL,update_date DATETIME NOT NULL);

CREATE TABLE recipeinsert(id SERIAL,imgpass varchar(255) NOT NULL,
foodname varchar(255) NOT NULL,requiredtime Integer,cost Integer,cal Integer,comments text,
servings Integer NOT NULL,create_date DATETIME NOT NULL,update_date DATETIME NOT NULL,
good Integer,great Integer,usersid Integer NOT NULL);

CREATE TABLE recipetag(id SERIAL ,recipe Integer NOT NULL,tagname varchar(255) NOT NULL);

CREATE TABLE material(id SERIAL,recipe Integer NOT NULL,material varchar(255) NOT NULL,
quantity varchar(255) NOT NULL);

CREATE TABLE process(id SERIAL,recipe Integer NOT NULL,processnumber Integer NOT NULL,
process varchar(255) NOT NULL,processimgpass varchar(255));

CREATE TABLE favorite(id SERIAL,recipe Integer NOT NULL,usersid Integer NOT NULL);

CREATE TABLE goodgreat(id SERIAL,recipe Integer NOT NULL,usersid Integer NOT NULL,good Integer,great Integer);

INSERT INTO users VALUE('admin', 'admin','admin111@nifty.com','password111','2019-12-11 11:11:00','2019-12-11 11:22:00');



ALTER TABLE recipeinsert MODIFY COLUMN update_date DATETIME;
ALTER TABLE recipeinsert MODIFY COLUMN requiredtime varchar(255);
ALTER TABLE recipeinsert MODIFY COLUMN cost varchar(255);
ALTER TABLE recipeinsert MODIFY COLUMN cal varchar(255);
ALTER TABLE recipeinsert MODIFY COLUMN servings varchar(255) NOT NULL;
ALTER TABLE process MODIFY COLUMN process varchar(255);
ALTER TABLE process MODIFY COLUMN processnumber varchar(255);

SELECT * FROM recipeinsert as i JOIN process as p ON i.id = p.recipe JOIN material as m ON p.recipe = m.recipe;

DELETE FROM users WHERE id=1;



