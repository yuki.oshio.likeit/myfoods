﻿SELECT
  i.*
  , m.material
  , t.tagname 
FROM
  recipeinsert as i JOIN material as m 
    ON i.id = m.recipe JOIN recipetag as t 
    ON m.recipe = t.recipe 
WHERE
  (foodname Like '%チャー%' 
  OR i.requiredtime Like '%チャー%' 
  OR i.cost Like '%チャー%' 
  OR i.cal Like '%チャー%' 
  OR t.tagname Like '%チャー%' 
  OR m.material Like '%チャー%' )
  AND create_date >= '2020-02-01'
ORDER BY
  create_date ASC

